import 'package:flutter/material.dart';

class MyColors
{
  static const List<LinearGradient> contentCardsGradients =
  [
    LinearGradient
    (
      colors: [ Color.fromRGBO(40, 48, 72, 1.0), Color.fromRGBO(133, 147, 152, 1.0) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color.fromRGBO(227, 42, 112, 1.0), Color.fromRGBO(255, 124, 0, 1.0) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color.fromRGBO(40, 48, 72, 1.0), Color.fromRGBO(214, 164, 164, 1.0) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color.fromRGBO(0, 70, 127, 1.0), Color.fromRGBO(165, 204, 130, 1.0) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color.fromRGBO(168, 192, 255, 1.0), Color.fromRGBO(63, 43, 150, 1.0) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFF0F2F0), Color(0xFF000C40) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFE8CBC0), Color(0xFF636FA4) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFDCE35B), Color(0xFF45B649) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFF3494E6), Color(0xFFEC6EAD) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFee0979), Color(0xFFff6a00) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFA770EF), Color(0xFFCF8BF3), Color(0xFFFDB99B) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFf4c4f3), Color(0xFFfc67fa) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFF00c3ff), Color(0xFFffff1c) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFff7e5f), Color(0xFFfeb47b) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),
    LinearGradient
    (
      colors: [ Color(0xFFa1c4fd), Color(0xFFc2e9fb) ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    ),
    LinearGradient
    (
      colors: [ Color(0xFF6a11cb), Color(0xFF2575fc) ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    ),
    LinearGradient
    (
      colors: [ Color(0xFFf78ca0), Color(0xFFf9748f), Color(0xFFfd868c), Color(0xFFfe9a8b) ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    ),
    LinearGradient
    (
      colors: [ Color(0xFF0ba360), Color(0xFF3cba92) ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    ),
    LinearGradient
    (
      colors: [ Color(0xFFf6d365), Color(0xFFfda085) ],
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
    ),



    /*LinearGradient
    (
      colors: [ Color(0xFF), Color(0xFF) ],
      begin: Alignment(-1.0, -1.0),
      end: Alignment(1.0, 1.0),
    ),*/
  ];
  
  static const LinearGradient searchBarColors = LinearGradient
  (
    colors: [ Color(0xFF02C78D), Color(0xFF00FBB1) ],
    begin: Alignment(-1.0, -1.0),
    end: Alignment(1.0, 1.0),
  );

  static final Color backgroundColor = DateTime.now().hour >= 20 || DateTime.now().hour < 8 ? Colors.grey[900] : Colors.white;
  static final Color importantTitle = DateTime.now().hour >= 20 || DateTime.now().hour < 8 ? Colors.white : Colors.black;
  static final Color negativeImportantTitle = DateTime.now().hour >= 20 || DateTime.now().hour < 8 ? Colors.black : Colors.white;
  static final Color accentColor = DateTime.now().hour >= 20 || DateTime.now().hour < 8 ? Colors.cyan[300] : Colors.pink;
  static final Color subimportantTitle = DateTime.now().hour >= 20 || DateTime.now().hour < 8 ? Colors.white70 : Colors.black38;
}