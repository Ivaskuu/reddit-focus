import 'dart:math';

import 'package:scoped_model/scoped_model.dart';

import '../logic/post.dart';
import '../logic/user_comment.dart';
import '../my_vars.dart';
import 'reddit_api.dart';
import 'mycolors.dart';

import 'dart:async';
import 'package:draw/draw.dart';
import 'package:html_unescape/html_unescape_small.dart';

enum FeedType { Hot, New, Top, Rising, Controversial }

/// When the keyboard pops up, the page is rebuilt and
/// a new [FeedModel] is created. To prevent this,
/// save the model at that moment and restore it.
/// The double stands for the scroll offset.
List<FeedPageModel> modelsHierarchyList = [];
int actualSubreddit = 0;

class FeedModel extends Model {
  /// Private variables
  List<Post> _posts = [];
  int _currentPostPos = 0;
  FeedType _feedType;
  String _subredditName;
  StreamSubscription _subscription;
  bool _postsListVisible = false;

  /// Public variables
  List<Post> get posts => _posts;
  int get currentPostPos => _currentPostPos;
  Post get actualPost => posts[currentPostPos];
  bool get initialized => posts != null && posts.length >= 3;
  FeedType get feedType => _feedType;
  String get subredditName => _subredditName;
  bool get postsListVisible => _postsListVisible;

  /// Loads posts from a reddit feed.
  ///
  /// Defaults to front page. To load a custom subreddit pass [subredditName]
  /// and [feedtype] that defaults to [FeedType.Hot]
  FeedModel([this._subredditName, this._feedType = FeedType.Hot]) {
    modelsHierarchyList.add(FeedPageModel(this));
    _loadUserFeed();
  }

  /// Public methods
  void nextPost() {
    doOnLastFeedModel(() {
      _currentPostPos++;
      notifyListeners();

      if (_posts.length - _currentPostPos < 4) {
        _subscription.resume();
      }
    });
  }

  /// Load a lot of posts for when the user swipes down and opens the posts list
  void loadPostsList() {
    doOnLastFeedModel(() {
      if (_posts.length - _currentPostPos < 50) {
        _subscription.resume();
      }
    });
  }

  void previousPost() {
    doOnLastFeedModel(() {
      if (_currentPostPos > 0) {
        _currentPostPos--;
        notifyListeners();
      }
    });
  }

  void upvotePost() {
    if (!MyVars.readOnly) {
      doOnLastFeedModel(() {
        // MyVars.analytics.logEvent(name: 'votePost', parameters: {'from': 'doubleTapCard'});

        Post post = _posts[_currentPostPos];
        if (post.voteStatus != null) {
          RedditApi.votePost(post.id, null);
          post.voteStatus == true ? post.upvotes-- : post.upvotes++;
          post.voteStatus = null;
        } else {
          RedditApi.votePost(post.id, true);
          post.voteStatus == false ? post.upvotes += 2 : post.upvotes++;
          post.voteStatus = true;
        }

        notifyListeners();
      });
    }
  }

  void postComment(UserComment newComment, {UserComment replyingTo}) {
    if (!MyVars.readOnly) {
      doOnLastFeedModel(() {
        if (replyingTo != null) {
          // Check if replying to a comment or a reply
          for (var comment in actualPost.comments) {
            if (comment.id == replyingTo.id) {
              // Replying to a comment
              newComment.depth = 1;
              comment.replies.insert(0, newComment);
              break;
            } else {
              // Replying to a reply of this comment
              if (comment.replies != null) {
                for (int i = 0; i < comment.replies.length; i++) {
                  if (comment.replies[i].id == replyingTo.id) {
                    newComment.depth = replyingTo.depth + 1;
                    comment.replies.insert(i + 1, newComment);
                    print('Replying to a reply [$i]');
                    break;
                  }
                }
              }
            }
          }
        } else {
          actualPost.comments.insert(0, newComment);
        }

        notifyListeners();
      });
    }
  }

  /// Sometimes there is a bug and the last opened feed updates an old one
  void doOnLastFeedModel(Function() fun) {
    if (modelsHierarchyList.last.feedModel.hashCode == this.hashCode) {
      print('doOnLasdt : ' + this.hashCode.toString());
      fun();
    } else {
      modelsHierarchyList.last.feedModel.doOnLastFeedModel(fun);
    }
  }

  void downloadMedia() {
    doOnLastFeedModel(() {
      actualPost.download = true;
      notifyListeners();
    });
  }

  void showNsfwMedia() {
    doOnLastFeedModel(() {
      actualPost.nsfw = false;
      notifyListeners();
    });
  }

  void showPostsList() {
    doOnLastFeedModel(() {
      _postsListVisible = true;
      notifyListeners();
    });
  }

  void closePostsList() {
    doOnLastFeedModel(() {
      _postsListVisible = false;
      notifyListeners();
    });
  }

  void goToPost(int ith) {
    doOnLastFeedModel(() {
      _currentPostPos = ith;
      notifyListeners();
    });
  }

  /// Private methods
  void _loadUserFeed() async {
    Stream<UserContent> subreddit;

    if (_subredditName == null) {
      subreddit = RedditApi.reddit.front.hot();
    } else {
      SubredditRef subredditRef = RedditApi.reddit.subreddit(_subredditName);
      switch (_feedType) {
        case FeedType.Hot:
          subreddit = subredditRef.hot();
          break;
        case FeedType.Top:
          subreddit = subredditRef.top();
          break;
        case FeedType.New:
          subreddit = subredditRef.newest();
          break;
        case FeedType.Rising:
          subreddit = subredditRef.rising();
          break;
        case FeedType.Controversial:
          subreddit = subredditRef.controversial();
          break;
        default:
          throw Exception('Unknown feed type: $_feedType');
          break;
      }
    }

    /// When [_subscription] is resumed it adds a new post in [_posts]
    _subscription = subreddit.listen((UserContent submissionn) {
      Submission s = submissionn as Submission;
      Post newPost = Post(s.title.toString(),
          selfText: s.selftext.toString(),
          url: s.url.toString().contains('www.reddit.com/r/')
              ? null
              : s.url.toString(),
          upvotes: s.upvotes,
          voteStatus: s.likes,
          opName: s.author,
          subredditName: s.subreddit.displayName,
          nsfw: s.over18,
          pinned: s.pinned,
          createdTime: s.createdUtc,
          commentsNum: s.numComments,
          gradientColor:
              Random().nextInt(MyColors.contentCardsGradients.length),
          id: s.id);
      _posts.add(newPost);

      loadPostComments(s).then((List<UserComment> postComments) {
        newPost.comments = postComments;
        notifyListeners();
      });

      if (_posts.length - _currentPostPos > 3 && !_subscription.isPaused) {
        _subscription.pause();
        if (_posts.length == 4) notifyListeners();
      }
    });
  }

  Future<List<UserComment>> loadPostComments(Submission s) async {
    List<UserComment> userComments = List();
    final HtmlUnescape unescape = HtmlUnescape();

    Submission sWithComments = await s.populate(); // Load comments

    for (var comm in sWithComments.comments.comments) {
      if (comm is Comment) {
        userComments.add(UserComment(
            unescape.convert(comm.body).replaceAll(r'&nbsp;', ' '),
            comm.author,
            comm.createdUtc,
            upvotesNum: comm.scoreHidden ? null : comm.upvotes,
            voteStatus: comm.likes,
            replies: loadCommentReplies(comm, 1),
            id: comm.id));
      } else if (comm is MoreComments) {
        userComments
            .add(MoreUserComment(userComments.last.depth)); // MoreComments
      }
    }

    return userComments;
  }

  List<Reply> loadCommentReplies(Comment c, int depth) {
    List<Reply> replies = List();
    final HtmlUnescape unescape = HtmlUnescape();

    if (c.replies != null) {
      List commentReplies = c.replies.comments;

      for (var reply in commentReplies) {
        if (reply is Comment) {
          replies.add(Reply(
              unescape.convert(reply.body).replaceAll(r'&nbsp;', ' '),
              reply.author,
              reply.createdUtc,
              depth,
              upvotesNum: reply.scoreHidden ? null : reply.upvotes,
              voteStatus: reply.likes,
              id: reply.id));
          replies.addAll(loadCommentReplies(reply, depth + 1));
        }
      }

      notifyListeners();
    }

    return replies;
  }
}

class FeedPageModel {
  final FeedModel feedModel;
  double scrollOffset;

  FeedPageModel(this.feedModel) : scrollOffset = 0.0;
}
