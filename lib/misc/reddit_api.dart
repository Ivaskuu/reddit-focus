import 'package:flutter/material.dart';
import 'package:ulid/ulid.dart';
import 'dart:async';
import 'package:draw/draw.dart';
import '../my_vars.dart';
import '../logic/post.dart';
import '../logic/user_comment.dart';

class RedditApi {
  static Reddit reddit;
  static Redditor me;
  static WebAuthenticator auth;

  static void initReddit() async {
    reddit = Reddit.createWebFlowInstance(
      clientId: 'N1Au0VDE5DQXRg',
      clientSecret: '',
      redirectUri: Uri.parse(
          r'https://v3pi0smvrkivz6xdjuzj1w-on.drv.tw/focus_reddit_login/'),
      userAgent: 'android:com.skuu.focusreddit:v0.1.0 (by /u/ivaskuu)',
    );
    auth = reddit.auth;
  }

  static Future initRedditWithCredentials(String credentialsJson) async {
    reddit = await Reddit.restoreAuthenticatedInstance(
      credentialsJson,
      clientId: 'N1Au0VDE5DQXRg',
      clientSecret: '',
      redirectUri: Uri.parse(
          r'https://v3pi0smvrkivz6xdjuzj1w-on.drv.tw/focus_reddit_login/'),
      userAgent: 'android:com.skuu.focusreddit:v0.0.1 (by /u/ivaskuu)',
    );

    updateUserInfos();
  }

  static Future initRedditNoCredentials() async {
    reddit = await Reddit.createUntrustedReadOnlyInstance(
      clientId: 'N1Au0VDE5DQXRg',
      deviceId: Ulid().toCanonical(),
      userAgent: 'android:com.skuu.focusreddit:v0.1.0 (by /u/ivaskuu)',
    );
  }

  static String getAuthUrl() {
    return auth.url([
      'edit',
      'account',
      'identity',
      'report',
      'privatemessages',
      'read',
      'save',
      'submit',
      'mysubreddits',
      'vote',
      'subscribe'
    ], 'RaNdOm_StRiNg', duration: 'permanent', compactLogin: true).toString();
  }

  static Future<bool> authorizeUser(String code) async {
    try {
      await auth.authorize(code);

      // Save credentials to the prefs
      MyVars.prefs.setString(PrefsVars.CredentialsJson.toString(),
          reddit.auth.credentials.toJson());
      MyVars.savedCredentials = true;

      updateUserInfos(newUser: true);
      return true;
    } catch (ex) {
      throw Exception('Error: $ex');
    }
  }

  static Future updateUserInfos({bool newUser}) async {
    print('Saving the user infos to the shared prefs');

    me = await reddit.user.me();

    if (newUser != null && newUser) {
      MyVars.analytics.setUserId(me.displayName);
      MyVars.prefs.setString(PrefsVars.DisplayName.toString(), me.displayName);
    }
    MyVars.prefs
        .setInt(PrefsVars.Karma.toString(), me.linkKarma + me.commentKarma);

    if (MyVars.prefs.getStringList(PrefsVars.SubbedSubreddits.toString()) ==
        null)
      MyVars.prefs
          .setStringList(PrefsVars.SubbedSubreddits.toString(), List<String>());
    List<String> subsSubscribed = List();

    StreamSubscription<Subreddit> subsStreamSubscription;
    subsStreamSubscription =
        reddit.user.subreddits().listen((Subreddit subreddit) {
      subsSubscribed.add(subreddit.displayName);
    });

    subsStreamSubscription.onDone(() {
      MyVars.prefs
          .setStringList(PrefsVars.SubbedSubreddits.toString(), subsSubscribed);
      subsStreamSubscription.cancel();
    });

    checkNewNotifications();
  }

  static void checkNewNotifications() {
    reddit.inbox.unread().listen((inboxData) {
      MyVars.notifications.add(inboxData);
    });
  }

  static Future getSubredditRules(String subreddit) async {
    List<Rule> rules = await reddit.subreddit(subreddit).rules();
    for (Rule rule in rules) {
      print(rule.description);
      print(rule.violationReason);
    }
  }

  static void subscribeSubreddit(String subreddit) =>
      reddit.subreddit(subreddit).subscribe();
  static void unsubscribeSubreddit(String subreddit) =>
      reddit.subreddit(subreddit).unsubscribe();

  static void votePost(String id, bool voteStatus) async {
    Submission s = await reddit.submission(id: id).populate();
    if (voteStatus == null)
      s.clearVote();
    else if (voteStatus)
      s.upvote();
    else
      s.downvote();
  }

  static void voteComment(String id, bool voteStatus) async {
    Comment c = await reddit.comment(id: id).populate();
    if (voteStatus == null)
      c.clearVote();
    else if (voteStatus)
      c.upvote();
    else
      c.downvote();
  }

  static Future<UserComment> postComment(String comment, String postId) async {
    Submission s = await reddit.submission(id: postId).populate();
    Comment reply = await s.reply(comment);

    return UserComment(reply.body, reply.author, reply.createdUtc,
        upvotesNum: 1, voteStatus: true, id: reply.id);
  }

  static Future<Reply> postReply(String comment, String commentId) async {
    CommentRef c = reddit.comment(id: commentId);

    // dyic9cn
    Comment cp = await c.populate();

    Comment reply = await cp.reply(comment);

    return Reply(reply.body, reply.author, reply.createdUtc, 0,
        upvotesNum: 1, voteStatus: true, id: reply.id);
  }

  static Future<Post> postSubmission(
      String subreddit, String title, String selftext, BuildContext context,
      {String url}) async {
    try {
      print(subreddit);
      print(title);
      print(selftext);

      Submission s =
          await reddit.subreddit(subreddit).submit(title, selftext: selftext);
      return Post(s.title.toString(),
          selfText: s.selftext.toString(),
          url: s.url.toString().contains('www.reddit.com/r/')
              ? null
              : s.url.toString(),
          upvotes: s.upvotes,
          voteStatus: s.likes,
          opName: s.author,
          subredditName: subreddit,
          pinned: s.pinned,
          createdTime: s.createdUtc,
          commentsNum: s.numComments,
          id: s.id);
    } catch (ex) {
      showDialog(
          context: context,
          child: AlertDialog(
            title: Text('The post couldn\'t be submitted.'),
            content: Text(ex.toString()),
          ));
      return null;
    }
  }
}
