import 'package:flutter/material.dart';
import 'splash_screen.dart';
import 'my_vars.dart';

void main() async {
  await MyVars.initPrefs();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MyVars.analytics.logAppOpen();

    return MaterialApp(
      title: 'Focus Reddit',
      theme: ThemeData(
        brightness: DateTime.now().hour >= 20 || DateTime.now().hour < 8
            ? Brightness.dark
            : Brightness.light,
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
      navigatorObservers: [MyVars.analyticsObserver],
      //showPerformanceOverlay: true,
    );
  }
}
