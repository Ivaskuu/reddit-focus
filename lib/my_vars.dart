import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:connectivity/connectivity.dart';
import 'logic/post.dart';

enum PrefsVars {
  CredentialsJson,
  DisplayName,
  Karma,
  SubbedSubreddits,
  ShowGitterDialog,
  CompletedTutorial,

  // Settings
  HideNSFW,
  SwipeToNextPostOnVote,
  PostCardsShadow,
  AutoDownloadMedia,

  // New things
  V009MessagesNew,
}

class MyVars {
  static SharedPreferences prefs;
  static bool savedCredentials;

  static bool readOnly = false;
  static String username;

  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver analyticsObserver =
      FirebaseAnalyticsObserver(analytics: analytics);

  static bool connectedToWifi;

  /// List of messages or mentions, to be shown in the navigation drawer
  static List<dynamic> notifications = List();

  static Future initPrefs() async {
    prefs = await SharedPreferences.getInstance();

    Connectivity connectivity = Connectivity();
    connectedToWifi =
        (await connectivity.checkConnectivity()) == ConnectivityResult.wifi;
    connectivity.onConnectivityChanged.listen((ConnectivityResult r) {
      connectedToWifi = r == ConnectivityResult.wifi;
    });

    if (getPrefsVar(PrefsVars.CredentialsJson) == null)
      savedCredentials = false;
    else
      savedCredentials = true;

    // Init null variables preferences
    if (getPrefsVar(PrefsVars.HideNSFW) == null)
      setPrefsVar(PrefsVars.HideNSFW, value: true);
    if (getPrefsVar(PrefsVars.SwipeToNextPostOnVote) == null)
      setPrefsVar(PrefsVars.SwipeToNextPostOnVote, value: false);
    if (getPrefsVar(PrefsVars.PostCardsShadow) == null)
      setPrefsVar(PrefsVars.PostCardsShadow, value: true);
    if (getPrefsVar(PrefsVars.AutoDownloadMedia) == null)
      setPrefsVar(PrefsVars.AutoDownloadMedia, value: false);

    username = getPrefsVar(PrefsVars.DisplayName);
  }

  static dynamic getPrefsVar(PrefsVars v) {
    return prefs.get(v.toString());
  }

  static void setPrefsVar(PrefsVars v, {bool value}) {
    prefs.setBool(v.toString(), value ?? !(getPrefsVar(v) as bool));
  }
}
