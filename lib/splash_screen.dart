import 'package:flutter/material.dart';

import 'pages/feed_page/feed_page.dart';
import 'misc/reddit_api.dart';
import 'my_vars.dart';
import 'dart:async';

class SplashScreen extends StatelessWidget {
  void checkForSavedCredentials(BuildContext context) async {
    try {
      if (MyVars.prefs == null) await MyVars.initPrefs();

      if (MyVars.savedCredentials) {
        await RedditApi.initRedditWithCredentials(
            MyVars.prefs.getString(PrefsVars.CredentialsJson.toString()));

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (_) => NewFeedPage()));
      } else {
        await RedditApi.initRedditNoCredentials();
        MyVars.readOnly = true;

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (_) => NewFeedPage()));
      }
    } catch (ex) {
      throw Exception(ex);
    }
  }

  @override
  Widget build(BuildContext context) {
    Timer(Duration(milliseconds: 100), () => checkForSavedCredentials(context));

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
    );
  }
}
