import 'package:flutter/material.dart';
import '../../misc/mycolors.dart';
import '../../misc/reddit_api.dart';
import 'package:draw/draw.dart';
import 'conversation_widget.dart';
import 'dart:async';

class CommentsPage extends StatefulWidget {
  @override
  _CommentsPageState createState() => new _CommentsPageState();
}

class _CommentsPageState extends State<CommentsPage>
    with AutomaticKeepAliveClientMixin {
  List<Comment> commentsList = List();
  StreamSubscription subscription;

  int actualMessage = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    subscription = RedditApi.reddit.inbox.all().listen((var comment) {
      if (comment is Comment) {
        setState(() => commentsList.add(comment));
        subscription.pause();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.backgroundColor,
      body: ListView.builder(
        padding: EdgeInsets.only(top: 8.0),
        itemBuilder: (_, int i) {
          if (i == actualMessage)
            _loadNewComment(); // When setState() is called it rebuilds all the widgets, which adds more widgets... not good...
          return ConversationWidget(comment: commentsList[i]);
        },
        itemCount: commentsList.length,
      ),
    );
  }

  void _loadNewComment() {
    actualMessage++;
    subscription.resume();
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }
}
