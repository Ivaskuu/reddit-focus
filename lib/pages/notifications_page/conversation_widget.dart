import 'package:flutter/material.dart';
import '../../misc/mycolors.dart';
import 'chat_page.dart';
import '../../my_vars.dart';
import 'package:draw/draw.dart';

class ConversationWidget extends StatefulWidget {
  final Message message;
  final Comment comment;

  ConversationWidget({this.message, this.comment})
      : super(key: Key(message != null ? message.id : comment.id)) {
    assert(message != null || comment != null);
  }

  @override
  ConversationWidgetState createState() {
    return new ConversationWidgetState();
  }
}

class ConversationWidgetState extends State<ConversationWidget> {
  bool newItem;

  @override
  void initState() {
    super.initState();
    if (widget.comment != null)
      newItem = widget.comment.newItem;
    else
      newItem = widget.message.newItem;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: widget.comment != null ? 0.0 : 8.0),
        child: InkWell(
            onTap: () {
              if (widget.message != null) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => ChatPage(widget.message.author,
                            Localizations.localeOf(context))));
              } else {
                setState(() {
                  widget.comment.markRead();
                  newItem = false;
                });

                // Remove notification from MyVars
                for (var n in MyVars.notifications) {
                  if (n.id == widget.comment.id) {
                    MyVars.notifications.remove(n);
                    break;
                  }
                }
              }
            },
            child: widget.comment != null
                ? CommentWidget(widget.comment, newItem)
                : MessageWidget(widget.message)));
  }
}

class CommentWidget extends StatelessWidget {
  final Comment c;
  final bool newItem;
  CommentWidget(this.c, this.newItem);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.reply, color: MyColors.importantTitle),
      title: newItem
          ? SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  CircleAvatar(backgroundColor: Colors.redAccent, radius: 4.0),
                  Padding(padding: EdgeInsets.only(right: 8.0)),
                  Text(c.author,
                      maxLines: 1,
                      overflow: TextOverflow.fade,
                      style: TextStyle(fontWeight: FontWeight.w700)),
                  Padding(padding: EdgeInsets.only(right: 8.0)),
                  Text('r/' + c.subreddit.displayName,
                      style: TextStyle(
                          color: MyColors.accentColor,
                          fontWeight: FontWeight.bold))
                ],
              ),
            )
          : SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: <Widget>[
                  Text(c.author),
                  Padding(padding: EdgeInsets.only(right: 8.0)),
                  Text('r/' + c.subreddit.displayName,
                      style: TextStyle(
                          color: MyColors.accentColor,
                          fontWeight: FontWeight.bold))
                ],
              ),
            ),
      subtitle: Text(c.body, maxLines: 2, overflow: TextOverflow.ellipsis),
    );
  }
}

class MessageWidget extends StatelessWidget {
  final Message m;
  MessageWidget(this.m);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8.0, vertical: 2.0),
      child: Material(
        color: MyColors.negativeImportantTitle,
        borderRadius: BorderRadius.circular(4.0),
        elevation: 6.0,
        shadowColor: Colors.black54,
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 8.0),
          child: ListTile(
            leading:
                Icon(Icons.account_circle, color: MyColors.subimportantTitle),
            title: m.newItem
                ? Row(
                    children: <Widget>[
                      CircleAvatar(
                          backgroundColor: Colors.redAccent, radius: 4.0),
                      Padding(padding: EdgeInsets.only(right: 8.0)),
                      Text(m.author,
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              color: MyColors.importantTitle)),
                    ],
                  )
                : Text(m.author, style: TextStyle(fontWeight: FontWeight.w700)),
            subtitle:
                Text(m.body, maxLines: 2, overflow: TextOverflow.ellipsis),
          ),
        ),
      ),
    );
  }
}
