import 'package:flutter/material.dart';
import '../../misc/mycolors.dart';
import 'messages_page.dart';
import 'comments_page.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => new _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        backgroundColor: MyColors.backgroundColor,
        title: Text('Notifications', style: TextStyle(color: MyColors.importantTitle, fontSize: 24.0, fontWeight: FontWeight.w700)),
        actions: <Widget>[
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.close, color: MyColors.subimportantTitle),
          )
        ],
        bottom: TabBar(
          controller: tabController,
          tabs: <Widget>[
            Tab(child: Text('Messages', style: TextStyle(color: MyColors.importantTitle))),
            Tab(child: Text('Comments', style: TextStyle(color: MyColors.importantTitle))),
          ],
        ),
      ),
      body: TabBarView(
        controller: tabController,
        children: <Widget>[
          MessagesPage(),
          CommentsPage(),
        ],
      ),
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
}