import 'package:flutter/material.dart';
import '../../misc/mycolors.dart';
import '../../misc/reddit_api.dart';
import 'package:draw/draw.dart';
import 'conversation_widget.dart';
import 'chat_page.dart';
import 'dart:async';

class MessagesPage extends StatefulWidget {
  @override
  _MessagesPageState createState() => new _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage>
    with AutomaticKeepAliveClientMixin {
  List<Message> messagesList = List();
  StreamSubscription subscription;

  int actualMessage = 0;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();

    subscription = RedditApi.reddit.inbox.all().listen((var message) {
      if (message is Message) {
        for (Message msg in messagesList) {
          if (msg.author == message.author) {
            //if(message.newItem) msg.newItem = true;
            return;
          }
        }

        setState(() => messagesList.add(message));
        subscription.pause();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.backgroundColor,
      body: ListView.builder(
        padding: EdgeInsets.only(top: 8.0),
        itemBuilder: (_, int i) {
          if (i == actualMessage)
            _loadNewComment(); // When setState() is called it rebuilds all the widgets, which adds more widgets... not good...

          return ConversationWidget(
              message: messagesList[i] is Message
                  ? messagesList[i]
                  : null /* , comment: messagesList[i] is Comment ? messagesList[i] : null */);
        },
        itemCount: messagesList.length,
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {},
          backgroundColor: MyColors.accentColor,
          child: Icon(Icons.add_comment, color: Colors.white)),
    );
  }

  void _loadNewComment() {
    actualMessage++;
    subscription.resume();
  }

  @override
  void dispose() {
    subscription.cancel();
    super.dispose();
  }
}
