import 'package:flutter/material.dart';
import '../../misc/mycolors.dart';
import 'package:draw/draw.dart';
import '../../misc/reddit_api.dart';
import '../../my_vars.dart';
import 'dart:async';

class ChatMessage {
  final String content;
  final bool fromUser;
  final DateTime createdUtc;

  const ChatMessage(this.content, this.fromUser, this.createdUtc);
}

Locale locale;

class ChatPage extends StatefulWidget {
  final String username;
  ChatPage(this.username, Locale userLocale) {
    locale = userLocale;
  }

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  List<ChatMessage> messages = List();
  TextEditingController controller = new TextEditingController();

  @override
  void initState() {
    super.initState();
    RedditApi.reddit.inbox.all().listen((m) {
      if (m is Message) {
        if (m.author == widget.username) {
          _addMessageToList(m, false);
          if (m.newItem) {
            m.markRead();

            // Remove notification from MyVars
            for (var n in MyVars.notifications) {
              if (n.id == m.id) {
                MyVars.notifications.remove(n);
                break;
              }
            }
          }
        }
      }
    });
    RedditApi.reddit.inbox.sent().listen((m) {
      if (m.destination == widget.username) _addMessageToList(m, true);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.backgroundColor,
        leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.arrow_back, color: MyColors.importantTitle)),
        centerTitle: true,
        title: Text('u/${widget.username}',
            style: TextStyle(
                color: MyColors.importantTitle,
                fontWeight: FontWeight.w600,
                fontSize: 20.0)),
        actions: <Widget>[
          Center(
              child: IconButton(
            onPressed: () {},
            icon: Icon(Icons.more_vert, color: MyColors.importantTitle),
          ))
        ],
      ),
      backgroundColor: MyColors.backgroundColor,
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              reverse: true,
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              itemBuilder: (_, int i) => ChatBubble(messages[i]),
              itemCount: messages.length,
            ),
          ),
          Container(
            margin: EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: TextField(
                  decoration: InputDecoration.collapsed(
                    hintText: 'Your new message...',
                  ),
                  maxLines: null,
                  controller: controller,
                )),
                FloatingActionButton(
                  onPressed: () => _sendComment(),
                  mini: true,
                  backgroundColor: MyColors.negativeImportantTitle,
                  child: Icon(Icons.send, color: MyColors.importantTitle),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  /// Adds a new Message to the list chronologically and calls setState()
  void _addMessageToList(Message m, bool fromUser) {
    for (int i = 0; i < messages.length; i++) {
      if (messages[i].createdUtc.compareTo(m.createdUtc) < 0) {
        setState(() =>
            messages.insert(i, ChatMessage(m.body, fromUser, m.createdUtc)));
        return;
      }
    }
    setState(() => messages.add(ChatMessage(m.body, fromUser, m.createdUtc)));
  }

  void _sendComment() {
    String msg = controller.text.trim();
    controller.clear();

    setState(() => messages.insert(0, ChatMessage(msg, true, DateTime.now())));

    /* RedditApi.reddit.redditor(widget.username).populate().then((Redditor redditor) {
      redditor.message('Message', msg);
      setState(() => messages.insert(0, ChatMessage(msg, true, DateTime.now())));
    }); */
  }
}

class ChatBubble extends StatelessWidget {
  final ChatMessage message;
  ChatBubble(this.message) : super(key: Key(message.toString()));

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: message.fromUser
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Flexible(
                child: _buildBubble(),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _buildBubble() {
    String date = '';

    if (locale.countryCode.toLowerCase() == 'us') {
      if (message.createdUtc.hour > 12) {
        date += '${message.createdUtc.hour - 12}:';
        if (message.createdUtc.minute < 10) date += '0';
        date += '${message.createdUtc.minute} PM ';
      } else {
        date += '${message.createdUtc.hour}:';
        if (message.createdUtc.minute < 10) date += '0';
        date += '${message.createdUtc.minute} AM ';
      }

      date += '${message.createdUtc.month}/${message.createdUtc.day}';
    } else {
      date += '${message.createdUtc.hour}:';
      if (message.createdUtc.minute < 10) date += '0';
      date += '${message.createdUtc.minute} ';

      date += '${message.createdUtc.day}/${message.createdUtc.month}';
    }
    if (message.createdUtc.year != DateTime.now().year)
      date += '/${message.createdUtc.year}';

    double radius = 10.0;

    if (message.fromUser) {
      return Container(
        margin: EdgeInsets.only(left: 64.0, bottom: 12.0),
        child: Material(
          elevation: 4.0,
          shadowColor: Colors.black54,
          color: Colors.blue,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(radius),
            bottomLeft: Radius.circular(radius),
            bottomRight: Radius.circular(radius),
          ),
          child: Container(
            padding: EdgeInsets.all(16.0),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    colors: [Colors.blue, Colors.indigo],
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(date, style: TextStyle(color: Colors.white70)),
                Text(message.content, style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
      );
    } else {
      return Container(
        margin: EdgeInsets.only(right: 64.0, bottom: 12.0),
        child: Material(
          elevation: 4.0,
          shadowColor: Colors.black54,
          color: MyColors.backgroundColor,
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(radius),
            bottomLeft: Radius.circular(radius),
            bottomRight: Radius.circular(radius),
          ),
          child: Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(date, style: TextStyle(color: MyColors.subimportantTitle)),
                Text(message.content,
                    style: TextStyle(color: MyColors.importantTitle)),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget _buildTimeText(DateTime chatDate) {
    return Text(chatDate.toString());
  }
}
