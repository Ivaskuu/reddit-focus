import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:draw/draw.dart';

import '../my_vars.dart';
import '../misc/mycolors.dart';
import '../misc/feed_model.dart';
import '../pages/feed_page/feed_page.dart';
import 'login_page.dart';
import 'settings_page.dart';
import 'notifications_page/notifications_page.dart';
import '../splash_screen.dart';

class NavigationDrawer extends StatefulWidget {
  @override
  NavigationDrawerState createState() => NavigationDrawerState();
}

class NavigationDrawerState extends State<NavigationDrawer> {
  @override
  Widget build(BuildContext context) {
    MyVars.analytics.logEvent(name: 'openNavDrawer');

    if (!MyVars.readOnly) {
      return Drawer(
        child: ListView(children: <Widget>[
          _buildMenuSection(context),
          MyVars.notifications.length > 0
              ? _buildNotificationsSection(context)
              : Container(),
          _buildSubredditsSection(context)
        ]),
      );
    } else {
      return Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(16.0),
              child: Text(
                  'Hey kind redditor! Thanks for using this app. If you are in incognito mode. If you want to login please click the button below.'),
            ),
            RaisedButton(
              onPressed: () => Navigator.pushReplacement(
                  context, MaterialPageRoute(builder: (_) => LoginPage())),
              child: Text('Login'),
            )
          ],
        ),
      );
    }
  }

  Widget _buildMenuSection(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: <
        Widget>[
      // User icon, name, karma, and logout button
      Container(
        margin: EdgeInsets.only(top: 24.0, left: 24.0, right: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.account_circle, color: Colors.grey, size: 64.0),
                  IconButton(
                    onPressed: () => _logOut(context),
                    icon: Icon(Icons.power_settings_new,
                        color: MyColors.accentColor, size: 32.0),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                          MyVars.prefs.getString(
                              PrefsVars.DisplayName.toString() ?? '[username]'),
                          style: TextStyle(
                              color: MyColors.importantTitle,
                              fontWeight: FontWeight.w700,
                              fontSize: 24.0)),
                      Text(
                          MyVars.prefs.getInt(PrefsVars.Karma.toString()) !=
                                  null
                              ? MyVars.prefs
                                      .getInt(PrefsVars.Karma.toString())
                                      .toString() +
                                  ' karma points'
                              : '[karma]',
                          style: TextStyle(
                              color: MyColors.accentColor,
                              fontSize: 14.0,
                              fontWeight: FontWeight.w700)),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      Padding(padding: EdgeInsets.only(bottom: 16.0)),
      ListTile(
        onTap: () {},
        leading: Icon(Icons.account_box),
        title: Text('My account'),
      ),
      ListTile(
        onTap: () {
          MyVars.prefs.setBool(PrefsVars.V009MessagesNew.toString(), true);
          Navigator
              .push(context,
                  MaterialPageRoute(builder: (_) => NotificationsPage()))
              .then((_) => setState(() {}));
        },
        leading: Icon(Icons.message),
        title: Text('Messages'),
        /* subtitle:
            MyVars.prefs.getBool(PrefsVars.V009MessagesNew.toString()) == null
                ? Text('NEW', style: TextStyle(color: MyColors.accentColor))
                : Container(), */
      ),
      ListTile(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => SettingsPage()));
        },
        leading: Icon(Icons.settings),
        title: Text('Settings'),
      ),
      Divider(),
    ]);
  }

  Widget _buildNotificationsSection(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.only(bottom: 12.0)),
          Padding(
            padding: EdgeInsets.only(left: 16.0, bottom: 8.0),
            child: Text('Notifications',
                style: TextStyle(
                    color: MyColors.accentColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 14.0)),
          ),
        ]
          ..addAll(
              MyVars.notifications.map((n) => _notificationWidget(n, context)))
          ..add(Divider()));
  }

  Widget _notificationWidget(dynamic notification, BuildContext context) {
    return ListTile(
        onTap: () => Navigator
            .push(
                context, MaterialPageRoute(builder: (_) => NotificationsPage()))
            .then((_) => setState(() {})),
        leading: Icon(notification is Message ? Icons.message : Icons.reply),
        title: Text('${notification.body}',
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: MyColors.importantTitle, fontWeight: FontWeight.w600)),
        subtitle: Text(notification is Message
            ? notification.author
            : (notification as Comment).author),
        trailing:
            CircleAvatar(backgroundColor: MyColors.accentColor, radius: 4.0));
  }

  Widget _buildSubredditsSection(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.only(bottom: 12.0)),
          Padding(
            padding: EdgeInsets.only(left: 16.0, bottom: 8.0),
            child: Text('My subreddits',
                style: TextStyle(
                    color: MyColors.accentColor,
                    fontWeight: FontWeight.w700,
                    fontSize: 14.0)),
          ),
          /* ListTile(
            onTap: () => null,// _showSubredditDialog(context),
            dense: true,
            leading: Icon(Icons.search),
            title: Text('Search for a subreddit'),
          ) */
        ]..addAll(_getSubbedSubredditsList(context)));
  }

  /* void _showSubredditDialog(BuildContext context) {
    TextEditingController subredditTextController = TextEditingController();

    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text('Enter the name of the subreddit'),
              content: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('r/'),
                    Padding(padding: EdgeInsets.only(right: 4.0)),
                    Expanded(
                        child: TextField(
                            controller: subredditTextController,
                            autofocus: true,
                            decoration: InputDecoration.collapsed(
                                hintText: 'The name of the subreddit')))
                  ]),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    MyVars.analytics.logEvent(
                        name: 'searchForSubreddit',
                        parameters: {
                          'subredditName': subredditTextController.text.trim()
                        });

                    Navigator.pop(context);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => NewFeedPage(
                                subreddit:
                                    subredditTextController.text.trim())));
                  },
                  child: Text('GO TO SUBREDDIT',
                      style: TextStyle(color: MyColors.accentColor)),
                )
              ],
            ));
  }
 */
  List<ListTile> _getSubbedSubredditsList(BuildContext context) {
    List<String> subsList =
        MyVars.prefs.getStringList(PrefsVars.SubbedSubreddits.toString()) ??
            List();

    List<ListTile> subsButtons = List();
    subsList.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));

    for (String sub in subsList) {
      subsButtons.add(ListTile(
        dense: true,
        onTap: () {
          MyVars.analytics.logEvent(
              name: 'goToSubreddit',
              parameters: {'subredditName': sub, 'from': 'navigationDrawer'});

          Navigator.push(context,
              MaterialPageRoute(builder: (_) => NewFeedPage(subreddit: sub)));
        },
        leading: Icon(Icons.layers),
        title: Text('r/' + sub),
      ));
    }

    return subsButtons;
  }

  void _logOut(BuildContext context) {
    MyVars.analytics.logEvent(name: 'logout');

    MyVars.savedCredentials = false;
    MyVars.prefs.setString(PrefsVars.CredentialsJson.toString(), null);
    modelsHierarchyList = [];
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (_) => SplashScreen()));
  }
}
