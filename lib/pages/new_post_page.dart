import 'package:flutter/material.dart';
import '../misc/reddit_api.dart';
import '../misc/mycolors.dart';
import '../logic/post.dart';
import '../my_vars.dart';
// import 'solo_post_page.dart';

class NewPostPage extends StatefulWidget {
  @override
  _NewPostPageState createState() => _NewPostPageState();
}

class _NewPostPageState extends State<NewPostPage> {
  String _subreddit;
  TextEditingController _titleTxt = TextEditingController();
  TextEditingController _textTxt = TextEditingController();

  bool _waitingForSubmission = false;

  @override
  void initState() {
    super.initState();
    MyVars.analytics.setCurrentScreen(screenName: 'NewPostPage');
    //RedditApi.search('flutter');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          elevation: 0.0,
          backgroundColor: MyColors.backgroundColor,
          automaticallyImplyLeading: false,
          actions: <Widget>[
            IconButton(
                onPressed: () => Navigator.pop(context),
                icon: Icon(Icons.close, color: MyColors.subimportantTitle)),
          ]),
      backgroundColor: MyColors.backgroundColor,
      body: Stack(
        children: <Widget>[
          ListView(children: <Widget>[
            InkWell(
              onTap: () => Navigator
                      .push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => SelectSubredditPage()))
                      .then((var subredditName) {
                    if (subredditName != null)
                      setState(() => _subreddit = subredditName.toString());
                    return false;
                  }),
              child: Container(
                margin: EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('Subreddit',
                            style: TextStyle(
                                color: MyColors.importantTitle,
                                fontSize: 28.0,
                                fontWeight: FontWeight.w700)),
                        Padding(padding: EdgeInsets.only(bottom: 8.0)),
                        _subreddit == null
                            ? Text('Tap on the arrow to choose a subreddit',
                                style: Theme
                                    .of(context)
                                    .textTheme
                                    .caption
                                    .copyWith(fontSize: 16.0))
                            : Text('r/' + _subreddit,
                                style: TextStyle(
                                    color: MyColors.accentColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 16.0))
                      ],
                    ),
                    Icon(Icons.arrow_forward, color: MyColors.importantTitle)
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.only(bottom: 24.0)),
            Container(
              margin: EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _subreddit != null
                      ? Text('Title of your post',
                          style: TextStyle(
                              color: MyColors.importantTitle,
                              fontSize: 28.0,
                              fontWeight: FontWeight.w700))
                      : Container(),
                  _subreddit != null
                      ? TextField(
                          maxLines: 1,
                          controller: _titleTxt,
                          decoration: InputDecoration(
                              hintText: 'Keep it relevant and concise...'),
                        )
                      : Container(),

                  Padding(padding: EdgeInsets.only(bottom: 48.0)),

                  _subreddit != null &&
                          _titleTxt.text != null &&
                          _titleTxt.text.trim() != ''
                      ? Text('Text of the post',
                          style: TextStyle(
                              color: MyColors.importantTitle,
                              fontSize: 28.0,
                              fontWeight: FontWeight.w700))
                      : Container(),
                  _subreddit != null &&
                          _titleTxt.text != null &&
                          _titleTxt.text.trim() != ''
                      ? Padding(padding: EdgeInsets.only(bottom: 8.0))
                      : Container(),

                  // Markdown elements buttons list
                  /*Container
                    (
                      margin: EdgeInsets.only(bottom: 8.0, left: 8.0),
                      child: SizedBox.fromSize
                      (
                        size: Size.fromHeight(40.0),
                        child: ListView
                        (
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: <Widget>
                          [
                            IconButton
                            (
                              tooltip: 'Title',
                              onPressed: () => _insertMarkdown(MarkdownAction.Title),
                              icon: Icon(Icons.title, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Subtitle',
                              onPressed: () => _insertMarkdown(MarkdownAction.Subtitle),
                              icon: Icon(Icons.text_fields, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Make the text bold',
                              onPressed: () => _insertMarkdown(MarkdownAction.Bold),
                              icon: Icon(Icons.format_bold, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Make the text italic',
                              onPressed: () => _insertMarkdown(MarkdownAction.Italic),
                              icon: Icon(Icons.format_italic, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Make the text strikethrough',
                              onPressed: () => _insertMarkdown(MarkdownAction.Strikethrough),
                              icon: Icon(Icons.format_strikethrough, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Quote something',
                              onPressed: () => _insertMarkdown(MarkdownAction.Quote),
                              icon: Icon(Icons.format_quote, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Inserts a clickable url link',
                              onPressed: () => _insertMarkdown(MarkdownAction.Link),
                              icon: Icon(Icons.link, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Insert a line of code',
                              onPressed: () => _insertMarkdown(MarkdownAction.Code),
                              icon: Icon(Icons.code, color: MyColors.importantTitle),
                            ),
                            IconButton
                            (
                              tooltip: 'Horizontal line',
                              onPressed: () => _insertMarkdown(MarkdownAction.HorizontalLine),
                              icon: Icon(Icons.remove, color: MyColors.importantTitle),
                            ),
                          ],
                        ),
                      )
                    ),*/

                  _subreddit != null &&
                          _titleTxt.text != null &&
                          _titleTxt.text.trim() != ''
                      ? TextField(
                          maxLines: null,
                          controller: _textTxt,
                          onChanged: (_) => setState(() {}),
                          decoration: InputDecoration.collapsed(
                              hintText: 'Write here the text of your post...'),
                        )
                      : Container(),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                ],
              ),
            ),
          ]),
          _waitingForSubmission
              ? SizedBox.expand(child: Container(color: Colors.black38))
              : Container(),
          _waitingForSubmission
              ? Center(child: CircularProgressIndicator())
              : Container(),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _subreddit != null &&
                _titleTxt.text != null &&
                _titleTxt.text.trim() != ''
            ? _postSubmission
            : null,
        backgroundColor: _subreddit != null &&
                _titleTxt.text != null &&
                _titleTxt.text.trim() != ''
            ? MyColors.importantTitle
            : MyColors.subimportantTitle,
        child: Icon(Icons.send, color: MyColors.backgroundColor),
      ),
    );
  }

  void _postSubmission() async {
    setState(() => _waitingForSubmission = true);

    Post _userPost = await RedditApi.postSubmission(
        _subreddit, _titleTxt.text.trim(), _textTxt.text.trim(), context);
    if (_userPost != null) {
      // Navigator.pushReplacement(
      //     context, MaterialPageRoute(builder: (_) => SoloPostPage(_userPost)));
    } else {
      setState(() => _waitingForSubmission = false);
    }
  }
}

class SelectSubredditPage extends StatefulWidget {
  @override
  _SelectSubredditPageState createState() => _SelectSubredditPageState();
}

class _SelectSubredditPageState extends State<SelectSubredditPage> {
  @override
  void initState() {
    super.initState();
    MyVars.analytics.setCurrentScreen(screenName: 'SelectSubredditPage');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: MyColors.backgroundColor,
          elevation: 0.0,
          title: Text('Select a subreddit',
              style: TextStyle(color: MyColors.importantTitle)),
          leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.arrow_back, color: MyColors.importantTitle),
          ),
        ),
        backgroundColor: MyColors.backgroundColor,
        body: ListView(children: <Widget>[
          // Search box
          Container(
            margin: EdgeInsets.all(16.0),
            child: Material(
              color: Color(0x11000000),
              borderRadius: BorderRadius.circular(8.0),
              child: Container(
                margin: EdgeInsets.all(16.0),
                child: TextField(
                    decoration: InputDecoration.collapsed(
                        hintText: 'Search a subreddit...')),
              ),
            ),
          ),
          // List of subreddits
          Column(children: _getSubbedSubredditsList())
        ]));
  }

  List<ListTile> _getSubbedSubredditsList() {
    List<String> subsList =
        MyVars.prefs.getStringList(PrefsVars.SubbedSubreddits.toString()) ??
            List();

    List<ListTile> subsButtons = List();
    subsList.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));

    for (String sub in subsList) {
      subsButtons.add(ListTile(
        dense: true,
        onTap: () {
          Navigator.pop(context, sub);
        },
        leading: Icon(Icons.layers),
        title: Text('r/' + sub,
            style: TextStyle(
                color: MyColors.importantTitle,
                fontWeight: FontWeight.w500,
                fontSize: 16.0)),
      ));
    }

    return subsButtons;
  }
}
