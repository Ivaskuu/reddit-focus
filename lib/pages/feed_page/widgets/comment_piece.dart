import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../../logic/user_comment.dart';
import '../../../misc/mycolors.dart';
import '../../../my_vars.dart';
import 'dart:math' as math;

class CommentPiece extends StatelessWidget {
  final UserComment comment;
  final String opName;
  final Function() onPress;
  final Function() onLongPress;
  final Function() onDoubleTap;

  CommentPiece(this.comment, this.opName,
      {this.onPress, this.onLongPress, this.onDoubleTap})
      : super(key: Key(comment.id));

  final TextStyle normalTextStyle =
      TextStyle(fontSize: 16.0, color: MyColors.importantTitle);

  @override
  Widget build(BuildContext context) {
    bool isReply = comment is Reply;

    if (comment is MoreUserComment) {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 6.0),
        child: Material(
          color: Colors.black,
          borderRadius: BorderRadius.circular(6.0),
          child: Center(
              child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text('MORE COMMENTS', style: TextStyle(color: Colors.white)),
          )),
        ),
      );
    } else {
      return InkWell(
        onTap: () => onPress(),
        onLongPress: () => onLongPress(),
        onDoubleTap: () => onDoubleTap(),
        child: Container(
          padding: EdgeInsets.only(
              left: 20.0,
              right: 20.0,
              top: isReply ? 0.0 : 16.0,
              bottom: isReply ? 0.0 : 8.0),
          child: comment.foldType == FoldType.Parent
              ? Container(
                  child: isReply
                      ? commentWithDepthLines // Reply with hierarchy lines
                          (
                          Text('[collapsed] ' + comment.content,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontStyle: FontStyle.italic)),
                          (comment as Reply).depth)
                      : Text // It is a parent comment, not a reply, so no depth lines to show
                          ('[collapsed] ' + comment.content,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontStyle: FontStyle.italic)))
              : comment.foldType == FoldType.Folded
                  ? Container()
                  : isReply
                      ? commentWithDepthLines(
                          commentPiece(), (comment as Reply).depth)
                      : commentPiece(),
        ),
      );
    }
  }

  Widget commentPiece() {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(comment.userName,
                          maxLines: 5,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              color: comment.userName.toLowerCase() ==
                                      opName.toLowerCase()
                                  ? MyColors.accentColor
                                  : comment.userName ==
                                          MyVars.username
                                      ? Colors.amber[800]
                                      : MyColors.importantTitle,
                              fontWeight: FontWeight.w800,
                              fontSize: 14.0)),
                      Text(' • ' + getCommentTime(comment.unixTime) + ' ago',
                          overflow: TextOverflow.fade,
                          style: TextStyle(fontSize: 16.0)),
                    ],
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.only(left: 8.0),
                  child: comment.upvotesNum != null
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                                comment.voteStatus == null
                                    ? '${comment.upvotesNum} '
                                    : comment.voteStatus
                                        ? '${comment.upvotesNum} '
                                        : '${comment.upvotesNum} ',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: MyColors.accentColor,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 16.0)),
                            Padding(padding: EdgeInsets.only(right: 4.0)),
                            Text(
                                comment.voteStatus == null
                                    ? '♡'
                                    : comment.voteStatus ? '❤' : '👎',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: MyColors.accentColor,
                                    fontSize: 16.0))
                          ],
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text('[hidden]',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: MyColors.accentColor,
                                    fontWeight: FontWeight.w800,
                                    fontSize: 16.0)),
                            Padding(padding: EdgeInsets.only(right: 4.0)),
                            Text(
                                comment.voteStatus == null
                                    ? '♡'
                                    : comment.voteStatus ? '❤' : '👎',
                                overflow: TextOverflow.fade,
                                style: TextStyle(
                                    color: MyColors.accentColor,
                                    fontSize: 16.0))
                          ],
                        ))
            ],
          ),
          MarkdownBody(
              data: comment.content,
              onTapLink: (String url) => launch(url),
              styleSheet: MarkdownStyleSheet(
                  p: normalTextStyle,
                  a: normalTextStyle.copyWith(
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                      fontWeight: FontWeight.w500),
                  h1: normalTextStyle.copyWith(
                      fontSize: 24.0, fontWeight: FontWeight.w700),
                  h2: normalTextStyle.copyWith(
                      fontSize: 22.0, fontWeight: FontWeight.w700),
                  h3: normalTextStyle.copyWith(
                      fontSize: 20.0, fontWeight: FontWeight.w600),
                  h4: normalTextStyle.copyWith(
                      fontSize: 18.0, fontWeight: FontWeight.w600),
                  h5: normalTextStyle.copyWith(
                      fontSize: 16.0, fontWeight: FontWeight.w500),
                  h6: normalTextStyle.copyWith(fontWeight: FontWeight.w500),
                  code: normalTextStyle,
                  strong: normalTextStyle.copyWith(fontWeight: FontWeight.bold),
                  em: normalTextStyle.copyWith(fontStyle: FontStyle.italic),
                  img: normalTextStyle,
                  blockquote:
                      normalTextStyle.copyWith(fontStyle: FontStyle.italic),
                  blockquoteDecoration: BoxDecoration(
                      color: Color(0x0A000000),
                      border: Border(
                          left: BorderSide(color: Colors.pink, width: 5.0))),
                  codeblockDecoration: BoxDecoration(color: Color(0x10000000)),
                  horizontalRuleDecoration: BoxDecoration(
                      border:
                          Border(top: BorderSide(color: Color(0x20000000)))),
                  blockSpacing: 16.0,
                  blockquotePadding: 16.0,
                  codeblockPadding: 16.0,
                  listIndent: 16.0))
        ]);
  }

  Widget commentWithDepthLines(Widget commentChild, int numContainers,
      {int totalDepth}) {
    if (totalDepth == null) totalDepth = numContainers;

    if (numContainers > 0) {
      return Container(
          decoration: BoxDecoration(
              border: Border(
                  left: BorderSide(
                      color:
                          DateTime.now().hour >= 20 || DateTime.now().hour < 8
                              ? Color.fromARGB(
                                  math.max(
                                      (255 - 35 * (totalDepth - numContainers))
                                          .round(),
                                      50),
                                  255,
                                  255,
                                  255,
                                ) // Correcting for human color vision
                              : Color.fromARGB(
                                  math.max(
                                      (255 - 50 * (totalDepth - numContainers))
                                          .round(),
                                      50),
                                  0,
                                  0,
                                  0),
                      width: 1.3))),
          child: Container(
              margin: const EdgeInsets.only(left: 8.0),
              child: commentWithDepthLines(commentChild, numContainers - 1,
                  totalDepth: totalDepth)));
    } else {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: commentChild,
      );
    }
  }

  String getCommentTime(DateTime epochTime) {
    if (epochTime != null) {
      Duration diff = DateTime.now().difference(epochTime);
      if (diff.inMinutes < 60)
        return diff.inMinutes.toString() + ' min';
      else if (diff.inHours < 24)
        return diff.inHours.toString() + 'h';
      else if (diff.inDays < 31)
        return diff.inDays.toString() + ' days';
      else
        return (diff.inDays / 30).truncate().toString() + ' months';
    }
    return '∞';
  }
}
