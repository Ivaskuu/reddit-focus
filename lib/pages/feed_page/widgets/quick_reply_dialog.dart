import 'package:flutter/material.dart';
import '../../../misc/mycolors.dart';
import '../../../misc/reddit_api.dart';
import '../../../logic/user_comment.dart';
import '../../../my_vars.dart';
import '../../../misc/feed_model.dart';

enum MarkdownAction {
  Title,
  Subtitle,
  Bold,
  Italic,
  Strikethrough,
  Quote,
  Link,
  Code,
  HorizontalLine
}

/// Needed beacuase the keyboard is obscuring the [TextField]
class _SystemPadding extends StatelessWidget {
  final Widget child;

  _SystemPadding({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var mediaQuery = MediaQuery.of(context);
    return new AnimatedContainer(
        padding: mediaQuery.viewInsets,
        duration: const Duration(milliseconds: 300),
        child: child);
  }
}

class QuickReplyDialog extends StatefulWidget {
  final FeedModel model;
  final UserComment replyingTo;
  QuickReplyDialog(this.model, {this.replyingTo});

  @override
  _QuickReplyDialogState createState() => _QuickReplyDialogState();
}

class _QuickReplyDialogState extends State<QuickReplyDialog> {
  bool showLoading = false;
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return _SystemPadding(
      child: !showLoading
          ? Align(
              alignment: FractionalOffset.bottomCenter,
              child: Material(
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                    widget.replyingTo != null
                        ? Container(
                            margin: EdgeInsets.all(16.0),
                            child: RichText(
                              textAlign: TextAlign.start,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                              text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                    text: '@' + widget.replyingTo.userName,
                                    style: TextStyle(color: Colors.blue)),
                                TextSpan(
                                    text: ' ' + widget.replyingTo.content,
                                    style: TextStyle(
                                        color: MyColors.subimportantTitle)),
                              ]),
                            ))
                        : Container(),
                    Container(
                        margin: EdgeInsets.all(8.0),
                        child: Row(children: <Widget>[
                          // Cancel commenting button
                          IconButton(
                              onPressed: _showConfirmDialog,
                              icon: Icon(Icons.close,
                                  color: MyColors.subimportantTitle)),
                          // Comment textfield
                          Expanded(
                              child: Container(
                                  padding: EdgeInsets.all(8.0),
                                  child: TextField(
                                      autofocus: true,
                                      maxLines: null,
                                      controller: controller,
                                      decoration: InputDecoration.collapsed(
                                          hintText:
                                              'Type your comment here...')))),
                          // Publish comment button
                          IconButton(
                              onPressed: () => postComment(),
                              icon: Icon(Icons.send,
                                  color: MyColors.importantTitle))
                        ])),
                    // Markdown elements buttons list
                    Container(
                        margin: EdgeInsets.only(bottom: 8.0, left: 8.0),
                        child: SizedBox.fromSize(
                            size: Size.fromHeight(40.0),
                            child: ListView(
                                shrinkWrap: true,
                                scrollDirection: Axis.horizontal,
                                children: <Widget>[
                                  IconButton(
                                    tooltip: 'Title',
                                    onPressed: () =>
                                        _insertMarkdown(MarkdownAction.Title),
                                    icon: Icon(Icons.title,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Subtitle',
                                    onPressed: () => _insertMarkdown(
                                        MarkdownAction.Subtitle),
                                    icon: Icon(Icons.text_fields,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Make the text bold',
                                    onPressed: () =>
                                        _insertMarkdown(MarkdownAction.Bold),
                                    icon: Icon(Icons.format_bold,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Make the text italic',
                                    onPressed: () =>
                                        _insertMarkdown(MarkdownAction.Italic),
                                    icon: Icon(Icons.format_italic,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Make the text strikethrough',
                                    onPressed: () => _insertMarkdown(
                                        MarkdownAction.Strikethrough),
                                    icon: Icon(Icons.format_strikethrough,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Quote something',
                                    onPressed: () =>
                                        _insertMarkdown(MarkdownAction.Quote),
                                    icon: Icon(Icons.format_quote,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Inserts a clickable url link',
                                    onPressed: () =>
                                        _insertMarkdown(MarkdownAction.Link),
                                    icon: Icon(Icons.link,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                    tooltip: 'Insert a line of code',
                                    onPressed: () =>
                                        _insertMarkdown(MarkdownAction.Code),
                                    icon: Icon(Icons.code,
                                        color: MyColors.importantTitle),
                                  ),
                                  IconButton(
                                      tooltip: 'Horizontal line',
                                      onPressed: () => _insertMarkdown(
                                          MarkdownAction.HorizontalLine),
                                      icon: Icon(Icons.remove,
                                          color: MyColors.importantTitle))
                                ])))
                  ])))
          : Center(child: CircularProgressIndicator()),
    );
  }

  void postComment() async {
    setState(() => showLoading = true);

    if (widget.replyingTo != null) {
      print('replying to someone');
      widget.model.postComment(
          await RedditApi.postReply(
              controller.text.trim(), widget.replyingTo.id),
          replyingTo: widget.replyingTo);

      Navigator.pop(context);
    } else {
      print('replying to the post');
      widget.model.postComment(await RedditApi.postComment(
          controller.text.trim(), widget.model.actualPost.id));

      Navigator.pop(context);
    }

    MyVars.analytics.logEvent(name: 'postComment');
  }

  void _insertMarkdown(MarkdownAction type) {
    MyVars.analytics.logEvent(
        name: 'markdownInComment',
        parameters: {'markdownType': type.toString()});

    String baseText = controller.text;

    String newText;
    if (controller.selection.baseOffset != -1) {
      newText = baseText.substring(0, controller.selection.baseOffset) +
          _markdownActionToText(type)[0] +
          baseText.substring(controller.selection.baseOffset,
              controller.selection.extentOffset) +
          _markdownActionToText(type)[1] +
          baseText.substring(
              controller.selection.extentOffset, baseText.length);
    } else {
      newText = _markdownActionToText(type)[0] +
          _markdownActionToText(type)[1] +
          baseText;
    }

    controller.text = newText;
  }

  List<String> _markdownActionToText(MarkdownAction action) {
    switch (action) {
      case MarkdownAction.Title:
        return ['\n# ', ''];
        break;
      case MarkdownAction.Subtitle:
        return ['\n### ', ''];
        break;
      case MarkdownAction.Bold:
        return ['** ', '**'];
        break;
      case MarkdownAction.Italic:
        return ['_ ', '_'];
        break;
      case MarkdownAction.Strikethrough:
        return [' ', ''];
        break;
      case MarkdownAction.Quote:
        return ['\n> ', ''];
        break;
      case MarkdownAction.Link:
        return ['[link title](', ')'];
        break;
      case MarkdownAction.Code:
        return ['`', '`'];
        break;
      case MarkdownAction.HorizontalLine:
        return ['\n---', '\n'];
        break;
      default:
        return null;
        break;
    }
  }

  void _showConfirmDialog() {
    if (controller.text.trim() != '') {
      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (_) => AlertDialog(
                title: Text('Confirm cancel writing'),
                content: Text(
                    'Are you sure you want to cancel commenting? You will loose what you have already written.'),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('EXIT',
                        style: TextStyle(color: MyColors.accentColor)),
                  ),
                  FlatButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text('CONTINUE WRITING',
                        style: TextStyle(color: MyColors.accentColor)),
                  ),
                ],
              ));
    } else {
      Navigator.pop(context);
    }
  }
}
