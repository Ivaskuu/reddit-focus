import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../../logic/post.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerWidget extends StatefulWidget
{
  final Post post;
  VideoPlayerWidget(this.post)
    /*: super(key: VideoPlayerWidget.savedVideoPlayedController != null
      && VideoPlayerWidget.savedVideoPlayedController.dataSource != post.url ? Key(post.url) : null)*/;

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();

  static VideoPlayerController savedVideoPlayedController;
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget>
{
  VideoPlayerController videoController;

  @override void initState()
  {
    super.initState();
    
    if(VideoPlayerWidget.savedVideoPlayedController != null
      && VideoPlayerWidget.savedVideoPlayedController.dataSource == widget.post.url)
        videoController = VideoPlayerWidget.savedVideoPlayedController;
    else
    {
      if(VideoPlayerWidget.savedVideoPlayedController != null) {
        VideoPlayerWidget.savedVideoPlayedController.dispose();
        VideoPlayerWidget.savedVideoPlayedController = null;
      }

      if(widget.post.url.contains('gfycat') && !widget.post.url.endsWith('.mp4'))
      {
        http.read(widget.post.url).then((String body)
        {
          widget.post.url = body.split('id="mp4Source" src="')[1].split('"')[0];

          videoController = VideoPlayerController.network(widget.post.url)
            ..addListener(() { if(mounted) setState(() {}); })
            ..setVolume(0.0)
            ..setLooping(true)
            ..initialize()
              .then((_) => videoController.play());

          VideoPlayerWidget.savedVideoPlayedController = videoController;
        });
      }
      else
      {
        videoController = VideoPlayerController.network(widget.post.url)
          ..addListener(() { if(mounted) setState(() {}); })
          ..setVolume(0.0)
          ..setLooping(true)
          ..initialize()
            .then((_) => videoController.play());
        
        VideoPlayerWidget.savedVideoPlayedController = videoController;
      }
    }
  }

  @override
  Widget build(BuildContext context)
  {
    if(videoController != null && videoController.value.size != null)
    {
      return AspectRatio
      (
        aspectRatio: videoController.value.aspectRatio,
        child: VideoPlayer(videoController)
      );
    }
    else
    {
      return Column
        (
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>
          [
            Icon(Icons.videocam, color: Colors.white, size: 80.0),
            Padding(padding: EdgeInsets.only(bottom: 16.0)),

            CircularProgressIndicator(),

            Padding(padding: EdgeInsets.only(bottom: 16.0)),
            Text('Loading video...', style: TextStyle(color: Colors.white))
          ],
        );
    }
  }
}