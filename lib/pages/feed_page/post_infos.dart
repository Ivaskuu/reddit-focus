import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:html_unescape/html_unescape_small.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../misc/feed_model.dart';
import '../../misc/mycolors.dart';
import 'feed_page.dart';

class PostInfos extends StatelessWidget {
  final HtmlUnescape unescape = HtmlUnescape();
  final TextStyle normalTextStyle =
      TextStyle(fontSize: 16.0, color: MyColors.importantTitle);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<FeedModel>(builder: (_, __, model) {
      if (model.initialized) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                /// Open a new page with this subreddit
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (_) => NewFeedPage(
                            subreddit: model.actualPost.subredditName)));
              },
              child: Container(
                  margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 8.0),
                  child: Text(
                      model.actualPost != null
                          ? 'r/' + model.actualPost.subredditName
                          : 'r/error',
                      style: TextStyle(
                          color: MyColors.accentColor,
                          fontWeight: FontWeight.w800,
                          fontSize: 16.0))),
            ),
            Container(
                margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 2.0),
                child: Text(model.actualPost.title,
                    style: TextStyle(
                        color: MyColors.importantTitle,
                        fontWeight: FontWeight.w600,
                        fontSize: 28.0))),
            Container(
                margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 2.0),
                child: Text(
                    model.actualPost.opName != null
                        ? 'u/' +
                            model.actualPost.opName +
                            ' • ' +
                            getSubmissionTime(model.actualPost.createdTime) +
                            ' ago'
                        : 'u/error',
                    style: Theme
                        .of(context)
                        .textTheme
                        .caption
                        .copyWith(fontSize: 16.0))),
            model.actualPost.selfText != null &&
                    model.actualPost.selfText.trim() != ''
                ? Container(
                    margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                    child: MarkdownBody(
                        data: unescape
                            .convert(model.actualPost.selfText)
                            .replaceAll(r'&nbsp;', ' '),
                        onTapLink: (String url) => launch(url),
                        styleSheet: MarkdownStyleSheet(
                            p: normalTextStyle,
                            a: normalTextStyle.copyWith(
                                color: Colors.blue,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.w500),
                            h1: normalTextStyle.copyWith(
                                fontSize: 24.0, fontWeight: FontWeight.w700),
                            h2: normalTextStyle.copyWith(
                                fontSize: 22.0, fontWeight: FontWeight.w700),
                            h3: normalTextStyle.copyWith(
                                fontSize: 20.0, fontWeight: FontWeight.w600),
                            h4: normalTextStyle.copyWith(
                                fontSize: 18.0, fontWeight: FontWeight.w600),
                            h5: normalTextStyle.copyWith(
                                fontSize: 16.0, fontWeight: FontWeight.w500),
                            h6: normalTextStyle.copyWith(
                                fontWeight: FontWeight.w500),
                            code: normalTextStyle,
                            strong: normalTextStyle.copyWith(
                                fontWeight: FontWeight.bold),
                            em: normalTextStyle.copyWith(fontStyle: FontStyle.italic),
                            img: normalTextStyle,
                            blockquote: normalTextStyle.copyWith(fontStyle: FontStyle.italic),
                            blockquoteDecoration: BoxDecoration(color: Color(0x0A000000), border: Border(left: BorderSide(color: Colors.pink, width: 5.0))),
                            codeblockDecoration: BoxDecoration(color: Color(0x10000000)),
                            horizontalRuleDecoration: BoxDecoration(border: Border(top: BorderSide(color: Color(0x20000000)))),
                            blockSpacing: 16.0,
                            blockquotePadding: 16.0,
                            codeblockPadding: 16.0,
                            listIndent: 16.0)),
                  )
                : Container(),
          ],
        );
      } else {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                margin: EdgeInsets.only(left: 24.0, right: 128.0, top: 8.0),
                child: Container(
                    color: Colors.black12,
                    child: SizedBox.fromSize(size: Size.fromHeight(16.0)))),
            Container(
                margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 8.0),
                child: Container(
                    color: Colors.black12,
                    child: SizedBox.fromSize(size: Size.fromHeight(16.0)))),
            Container(
                margin: EdgeInsets.only(left: 24.0, right: 64.0, top: 8.0),
                child: Container(
                    color: Colors.black12,
                    child: SizedBox.fromSize(size: Size.fromHeight(16.0)))),
            Container(
                margin: EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0),
                child: Container(
                    color: Colors.black12,
                    child: SizedBox.fromSize(size: Size.fromHeight(16.0)))),
          ],
        );
      }
    });
  }

  String getSubmissionTime(DateTime epochTime) {
    if (epochTime != null) {
      Duration diff = DateTime.now().difference(epochTime);
      if (diff.inMinutes < 60)
        return diff.inMinutes.toString() + ' min';
      else if (diff.inHours < 24)
        return diff.inHours.toString() + 'h';
      else if (diff.inDays < 31)
        return diff.inDays.toString() + ' days';
      else
        return (diff.inDays / 30).truncate().toString() + ' months';
    }
    return '∞';
  }
}
