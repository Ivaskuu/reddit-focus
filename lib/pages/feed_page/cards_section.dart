import 'package:flutter/material.dart';
import 'dart:async';
import 'package:scoped_model/scoped_model.dart';

import '../../misc/reddit_api.dart';
import '../../my_vars.dart';
import '../../misc/feed_model.dart';
import 'post_card.dart';

import 'package:focus_reddit/misc/mycolors.dart';
import 'package:focus_reddit/logic/post.dart';

import 'dart:math';

List<double> cardsSizes = [314.0, 280.0, 250.0];
List<Alignment> cardsAlign = [
  Alignment(0.0, 0.0),
  Alignment(0.0, -0.8),
  Alignment(0.0, -1.0),
];

/// Used to rotate the [PostCard] on top when the user is swiping
double frontCardRot = 0.0;

/// The 3 tinder-like [PostCard]s
final List<PostCard> cards = List(3);

/// Cool-down time for the animation
bool canSwipe = true;

/// If true, when the user votes a post, it automatically swipes to the next one
bool _swipeToNextPostOnVote;

FeedModel model;

class CardsSection extends StatefulWidget {
  CardsSection(FeedModel m) {
    for (int i = 0; i < 3; i++) {
      cards[i] = PostCard(
          i,
          m.currentPostPos + i < m.posts.length
              ? m.posts[m.currentPostPos + i].gradientColor
              : null);
    }

    model = m;
    _swipeToNextPostOnVote =
        MyVars.prefs.getBool(PrefsVars.SwipeToNextPostOnVote.toString()) ??
            false;
  }

  @override
  CardsSectionState createState() => CardsSectionState();
}

class CardsSectionState extends State<CardsSection>
    with SingleTickerProviderStateMixin {
  /// Used to move the card on to when the user is swiping
  Alignment frontCardAlign = cardsAlign[0];
  AnimationController controller;
  bool orderChanged = false;

  @override
  void initState() {
    super.initState();
    MyVars.analytics.logEvent(name: 'newFrontCard');

    _initAnimationController();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant(
        builder: (_, __, model) => SizedBox.fromSize(
            key: widget.key,
            size: Size.fromHeight(340.0),
            child: Stack(children: <Widget>[
              // Cards
              // Hero(tag: widget.cards[0].post.id, child: backCard(0)),
              backCard(2),
              /* Hero(
                  tag: widget.cards[1].post.id,
                  child: backCard(1),
                ), */
              backCard(1),
              // Hero(tag: widget.cards[2].post.id, child: frontCard()),
              frontCard(),

              // Cards swipe region
              canSwipe
                  ? SizedBox.expand(
                      child: GestureDetector(
                      // While dragging the first card
                      onPanUpdate: (DragUpdateDetails details) {
                        setState(() {
                          // Pointer to alignment: 2 * (details.globalPosition.dx / MediaQuery.of(context).size.width - 0.5)
                          frontCardAlign = Alignment(
                              frontCardAlign.x +
                                  20 *
                                      details.delta.dx /
                                      MediaQuery.of(context).size.width,
                              frontCardAlign.y +
                                  40 *
                                      details.delta.dy /
                                      MediaQuery.of(context).size.height);

                          frontCardRot = (frontCardAlign.x / 20.0) *
                              20.0; // 40 the constant speed multiplier for the position
                        });
                      },
                      // When releasing the first card
                      onPanEnd: (_) {
                        // If the front card was swiped far enough
                        if (frontCardAlign.x > 3.0 || frontCardAlign.x < -3.0) {
                          animateCards();

                          MyVars.analytics.logEvent(name: 'cardsSwipe');
                        } else // If the front card wasn't swiped far enough
                        {
                          // Return to the initial rotation and alignment, fake alarm
                          setState(() {
                            frontCardAlign = Alignment(0.0, 0.0);
                            frontCardRot = 0.0;
                          });
                        }
                      },
                    ))
                  : Container(),
              likesFab()
            ])));
  }

  void changeCardsOrder() {
    // Firstly, the cards animate and place to their next position (alignment and size)
    // then, right when the animation finishes, set the cards position

    modelsHierarchyList.last.feedModel.nextPost();

    setState(() {
      frontCardAlign = Alignment(0.0, 0.0);
      frontCardRot = 0.0;
    });

    MyVars.analytics.logEvent(
        name: 'newFrontCard',
        parameters: {'autoSwipe': _swipeToNextPostOnVote});
    canSwipe = true;
  }

  Widget backCard(int ith) {
    if (controller.status == AnimationStatus.forward) {
      if (false) //if(ith == 0 && controller.value >= 0.7) // The front card, that is now the back card, appearing
      {
        return Align(
          alignment: cardsAlign[ith],
          child: SizedBox.fromSize(
              size: CardsAnimation.frontCardAppearSizeAnim(controller).value,
              child: cards[ith]),
        );
      } else // The back card going to the middle or the middle card going to the top
      {
        return Align(
          alignment:
              CardsAnimation.backCardAlignmentAnim(controller, ith).value,
          child: SizedBox.fromSize(
              size: CardsAnimation
                  .backCardSizeAnim(controller, ith)
                  .value, // value < 0.7
              child: cards[ith]),
        );
      }
    } else // Not animating
    {
      return Align(
        alignment: cardsAlign[ith],
        child: SizedBox.fromSize(
            size: Size(
                cardsSizes[ith], cardsSizes[ith]), // If not animating cards
            child: cards[ith]),
      );
    }
  }

  Widget frontCard() {
    return Align(
        alignment: controller.status == AnimationStatus.forward
            ? CardsAnimation
                .frontCardDisappearAlignmentAnim(controller, frontCardAlign)
                .value
            : frontCardAlign,
        child: Transform.rotate(
            angle: (pi / 180.0) * frontCardRot,
            child: SizedBox.fromSize(
                size: Size(cardsSizes[0], cardsSizes[0]), child: cards[0])));
  }

  Widget likesFab() {
    return ScopedModelDescendant<FeedModel>(builder: (_, __, model) {
      if (model.posts != null && model.posts.length > 0) {
        return Align(
            alignment: Alignment(1.0, 1.0),
            child: Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Material(
                    elevation: 4.0,
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(64.0),
                    child: InkWell(
                        onTap: () => !MyVars.readOnly ? showOptions() : null,
                        child: CircleAvatar(
                            radius: cardsAlign[2].x == 0.0 ? 42.0 : 0.0,
                            backgroundColor:
                                model.posts[model.currentPostPos].voteStatus == null
                                    ? MyColors.accentColor
                                    : model.posts[model.currentPostPos].voteStatus == false
                                        ? Colors.red
                                        : Colors.green,
                            child: Transform.rotate(
                                angle: 0.17,
                                child: Text(
                                    model.posts[model.currentPostPos].upvotes
                                        .toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontSize: 24.0))))))));
      } else {
        return Align(
            alignment: Alignment(1.0, 1.0),
            child: Container(
                margin: EdgeInsets.only(right: 8.0),
                child: Material(
                    elevation: 4.0,
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(64.0),
                    child: InkWell(
                        onTap: showOptions,
                        child: CircleAvatar(
                            radius: cardsAlign[2].x == 0.0 ? 42.0 : 0.0,
                            backgroundColor: MyColors.accentColor)))));
      }
    });
  }

  /// Starts the cards animation (to the next card)
  void animateCards() {
    // Prevente the user to swipe while the animation is running
    setState(() => canSwipe = false);

    controller.stop();
    controller.value = 0.0;
    controller.forward();
  }

  void showOptions() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            color: MyColors.backgroundColor,
            child: Material(
              color: Colors.transparent,
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  InkWell(
                      // TODO: onTap: () { MyVars.analytics.logEvent(name: 'votePost', parameters: { 'from': 'optionsDialog' }); Navigator.pop(context); voteLastPost(true); },
                      child: SwitchListTile(
                    title: Text('Swipe to next post on vote',
                        style: TextStyle(fontSize: 16.0)),
                    value: _swipeToNextPostOnVote,
                    dense: true,
                    onChanged: (bool value) {
                      MyVars.prefs.setBool(
                          PrefsVars.SwipeToNextPostOnVote.toString(), value);
                      setState(() {
                        _swipeToNextPostOnVote = value;
                      });
                      Navigator.pop(context);
                      showOptions();
                    },
                  )),
                  InkWell(
                    onTap: () {
                      MyVars.analytics.logEvent(
                          name: 'votePost',
                          parameters: {'from': 'optionsDialog'});
                      Navigator.pop(context);
                      model.upvotePost();
                    },
                    child: ListTile(
                      leading: Icon(Icons.favorite),
                      title: Text('Upvote'),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      MyVars.analytics.logEvent(
                          name: 'votePost',
                          parameters: {'from': 'optionsDialog'});
                      Navigator.pop(context);
                      model.upvotePost();
                    },
                    child: ListTile(
                      leading: Icon(Icons.swap_vertical_circle),
                      title: Text('Clear vote'),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      MyVars.analytics.logEvent(
                          name: 'votePost',
                          parameters: {'from': 'optionsDialog'});
                      Navigator.pop(context);
                      // voteLastPost(false);
                    },
                    child: ListTile(
                      leading: Icon(Icons.arrow_downward),
                      title: Text('Downvote'),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: ListTile(
                      leading: Icon(Icons.save),
                      title: Text('Save post'),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: ListTile(
                      leading: Icon(Icons.flag),
                      title: Text('Report'),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _initAnimationController() {
    controller =
        AnimationController(duration: Duration(milliseconds: 700), vsync: this);
    controller.addListener(() {
      setState(() {
        // TODO: At middle animation, make the frontcard (now backcard) smoothly appear
        /*if(controller.value >= 0.7 && !orderChanged)
        {
          print('tuo padre');

          changeCardsOrder();
          orderChanged = true;
        }*/
      });
    });
    controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed)
        changeCardsOrder(); //orderChanged = false;
    });
  }
}

class CardsAnimation {
  static Animation<Alignment> backCardAlignmentAnim(
      AnimationController parent, int pos) {
    return AlignmentTween(begin: cardsAlign[pos], end: cardsAlign[pos - 1])
        .animate(CurvedAnimation(
            parent: parent,
            curve: Interval(0.4 - pos * 0.2, 0.7 - pos * 0.2,
                curve: Curves.easeIn)));
  }

  static Animation<Size> backCardSizeAnim(AnimationController parent, int pos) {
    return SizeTween(
            begin: Size(cardsSizes[pos], cardsSizes[pos]),
            end: Size(cardsSizes[pos - 1], cardsSizes[pos - 1]))
        .animate(CurvedAnimation(
            parent: parent,
            curve: Interval(0.4 - pos * 0.2, 0.7 - pos * 0.2,
                curve: Curves.easeIn)));
  }

  static Animation<Alignment> frontCardDisappearAlignmentAnim(
      AnimationController parent, Alignment beginAlign) {
    return AlignmentTween(
            begin: beginAlign,
            end: Alignment(
                beginAlign.x > 0 ? beginAlign.x + 30.0 : beginAlign.x - 30.0,
                0.0))
        .animate(CurvedAnimation(
            parent: parent, curve: Interval(0.0, 0.5, curve: Curves.easeIn)));
  }

  static Animation<Size> frontCardAppearSizeAnim(AnimationController parent) {
    return SizeTween(
            begin: Size(10.0, 10.0), end: Size(cardsSizes[0], cardsSizes[0]))
        .animate(CurvedAnimation(
            parent: parent, curve: Interval(0.7, 1.0, curve: Curves.easeIn)));
  }
}
