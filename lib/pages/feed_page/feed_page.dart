import 'dart:async';

import 'package:flutter/material.dart';
import 'package:focus_reddit/pages/login_page.dart';
import 'package:focus_reddit/pages/new_post_page.dart';
import 'package:focus_reddit/pages/tutorial_page.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../my_vars.dart';
import '../search_page.dart';
import '../navigation_drawer.dart';
import '../../misc/mycolors.dart';
import 'widgets/quick_reply_dialog.dart';
import '../../misc/feed_model.dart';
import 'cards_section.dart';
import 'post_infos.dart';
import 'comments_section.dart';
import '../posts_list_page.dart';

class NewFeedPage extends StatelessWidget {
  final FeedModel m;
  final PostsListPage postsListPage = PostsListPage(null);

  final String subreddit;
  final FeedType feedType;

  ScrollController controller;
  final int posInHierarchy;

  double overscrollDist;

  /// If there are no models create one. Otherwise load the model already
  /// existing or create a new one (when opening a new subreddit)
  NewFeedPage({this.subreddit, this.feedType: FeedType.Hot})
      : m = modelsHierarchyList.length == 0 // The first feed
            ? FeedModel(subreddit, feedType)
            : modelsHierarchyList.firstWhere(
                        (mm) => mm.feedModel.subredditName == subreddit,
                        orElse: () => null) !=
                    null
                ? modelsHierarchyList
                    .firstWhere((mm) => mm.feedModel.subredditName == subreddit)
                    .feedModel
                : FeedModel(subreddit, feedType),
        posInHierarchy = modelsHierarchyList.indexOf(modelsHierarchyList
            .firstWhere((mm) => mm.feedModel.subredditName == subreddit)) {
    controller = ScrollController(
        initialScrollOffset:
            modelsHierarchyList[posInHierarchy].scrollOffset ?? 0.0);
  }

  @override
  Widget build(BuildContext context) {
    GlobalKey<ScaffoldState> key = GlobalKey<ScaffoldState>();
    Timer(Duration(seconds: 2), () => showTutorialSnackbar(key.currentState));

    if (m != null) {
      return WillPopScope(
          onWillPop: () {
            if (modelsHierarchyList.last.feedModel.postsListVisible) {
              modelsHierarchyList.last.feedModel.closePostsList();
              return Future<bool>.value(false);
            } else {
              modelsHierarchyList.removeLast();
              return Future<bool>.value(true);
            }
          },
          child: ScopedModel<FeedModel>(
              model: m,
              child: Scaffold(
                  key: key, // Needed to open the drawer in the sliver appBar
                  resizeToAvoidBottomPadding: false,
                  backgroundColor: MyColors.backgroundColor,
                  drawer: !MyVars.readOnly ? NavigationDrawer() : null,
                  body: NotificationListener(
                      onNotification: (notification) {
                        if (notification is OverscrollNotification) {
                          if (notification.dragDetails != null) {
                            // dragDetails is null if swiped fast
                            overscrollDist ??= notification
                                .dragDetails.globalPosition.distance;

                            if (notification
                                        .dragDetails.globalPosition.distance -
                                    overscrollDist >
                                160) {
                              m.showPostsList();
                            }
                          }
                        }
                        if (notification is UserScrollNotification ||
                            notification is ScrollEndNotification)
                          overscrollDist = null;

                        // Scroll to the last postion of the scroll view
                        modelsHierarchyList[posInHierarchy].scrollOffset =
                            controller.offset;
                      },
                      child: Stack(children: <Widget>[
                        CustomScrollView(
                            controller: controller,
                            slivers: <Widget>[
                              _buildSliverAppBar(context, key),
                              SliverToBoxAdapter(
                                child: ScopedModelDescendant<FeedModel>(
                                    builder: (context, child, model) {
                                  return Column(
                                    children: <Widget>[
                                      CardsSection(m),
                                      PostInfos(),
                                    ],
                                  );
                                }),
                              ),
                              CommentsSection(),
                              SliverToBoxAdapter(child: SizedBox(height: 64.0))
                            ]),
                        PostsListPage(m)
                      ])),
                  floatingActionButton:
                      !MyVars.readOnly && !modelsHierarchyList.last.feedModel.postsListVisible
                          ? FloatingActionButton(
                              backgroundColor: Colors.yellow,
                              child: Icon(Icons.edit, color: Colors.black),
                              onPressed: () => showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (_) {
                                    return Align(
                                        alignment: Alignment.bottomCenter,
                                        child: QuickReplyDialog(m));
                                  }))
                          : null)));
    } else {
      throw Exception(
          'The feed model is null, and that doesn\'t seem very normal.');
    }
  }

  /// The [AppBar] is different wether it's the frontpage or a subreddit
  SliverAppBar _buildSliverAppBar(
      BuildContext context, GlobalKey<ScaffoldState> key) {
    if (subreddit == null) {
      return SliverAppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        leading: null,
        backgroundColor: MyColors.backgroundColor,
        title: Text(
          'My Feed',
          style: TextStyle(
            color: MyColors.importantTitle,
            fontWeight: FontWeight.w800,
            fontSize: 30.0,
          ),
        ),
        actions: <Widget>[
          !MyVars.readOnly
              ? IconButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => NewPostPage()));
                  },
                  // onPressed: () => Navigator.push(context,
                  //     MaterialPageRoute(builder: (_) => NewPostPage())),
                  icon: Icon(Icons.edit, color: MyColors.importantTitle),
                )
              : Container(),
          /* IconButton(
            onPressed: () => _showSubredditDialog(context),
            icon: Icon(Icons.search, color: MyColors.importantTitle),
          ), */
          IconButton(
            onPressed: () {
              if (!MyVars.readOnly)
                key.currentState.openDrawer();
              else {
                showLoginSnackbar(key.currentState);
              }
            },
            icon: Icon(Icons.account_circle, color: MyColors.importantTitle),
          ),
        ],
        floating: true,
        snap: false,
      );
    } else {
      return SliverAppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        leading: null,
        backgroundColor: MyColors.backgroundColor,
        title: Text(
          'r/$subreddit',
          overflow: TextOverflow.fade,
          style: TextStyle(
            color: MyColors.importantTitle,
            fontWeight: FontWeight.w800,
            fontSize: 30.0,
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (_) {
                    Widget feedTypeTile(FeedType _feedType, IconData icon) =>
                        ListTile(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => NewFeedPage(
                                          subreddit: subreddit,
                                          feedType: _feedType)));
                            },
                            dense: true,
                            leading: Icon(icon),
                            title: Text(_feedType.toString().split('.')[1]));

                    return SimpleDialog(
                      title: Text('Filter posts'),
                      contentPadding: EdgeInsets.all(10.0),
                      children: <Widget>[
                        feedTypeTile(FeedType.Hot, Icons.whatshot),
                        feedTypeTile(FeedType.New, Icons.fiber_new),
                        feedTypeTile(FeedType.Top, Icons.arrow_upward),
                        feedTypeTile(FeedType.Rising, Icons.trending_up),
                        feedTypeTile(
                            FeedType.Controversial, Icons.question_answer),
                      ],
                    );
                  });
            },
            icon: Icon(Icons.sort, color: MyColors.importantTitle),
          ),
          /* IconButton(
            onPressed: () {
              if (!subscribedToSubreddit) {
                MyVars.prefs.setStringList(
                    PrefsVars.SubbedSubreddits.toString(),
                    MyVars.prefs
                        .getStringList(PrefsVars.SubbedSubreddits.toString())
                          ..add(widget.subreddit));
                RedditApi.subscribeSubreddit(widget.subreddit);
              } else {
                MyVars.prefs.setStringList(
                    PrefsVars.SubbedSubreddits.toString(),
                    MyVars.prefs
                        .getStringList(PrefsVars.SubbedSubreddits.toString())
                          ..remove(widget.subreddit));
                RedditApi.unsubscribeSubreddit(widget.subreddit);
              }
              setState(() => subscribedToSubreddit = !subscribedToSubreddit);
            },
            icon: Icon(
                subscribedToSubreddit
                    ? Icons.remove_circle_outline
                    :
                Icons.add_circle_outline,
                color: MyColors.importantTitle),
          ),*/
          /* IconButton(
            onPressed: () async {
              // TODO
              // MyVars.analytics.logEvent(name: 'subredditInfos');

              String rules =
                  await RedditApi.getSubredditRules(widget.subreddit);
              showDialog(
                  context: context,
                  child: AlertDialog(
                    title: Text(widget.subreddit),
                    content: Text(rules.toString()),
                  ));
            },
            icon: Icon(Icons.info_outline, color: MyColors.importantTitle),
          ),*/
          IconButton(
              onPressed: () {
                modelsHierarchyList.removeLast();
                Navigator.pop(context);
              },
              icon: Icon(Icons.close, color: MyColors.importantTitle)),
        ],
        floating: true,
        snap: false,
      );
    }
  }

  void showLoginSnackbar(ScaffoldState state) {
    state.showSnackBar(SnackBar(
      content: Text(
          'Log in to comment, upvote, see your profile, your notifications and your subreddits!'),
      duration: Duration(seconds: 10),
      action: SnackBarAction(
        label: 'LOGIN PAGE',
        onPressed: () => Navigator.pushReplacement(
            state.context, MaterialPageRoute(builder: (_) => LoginPage())),
      ),
    ));
  }

  void showTutorialSnackbar(ScaffoldState state) {
    print(MyVars.prefs.getBool(PrefsVars.CompletedTutorial.toString()));
    if (MyVars.prefs.getBool(PrefsVars.CompletedTutorial.toString()) != true) {
      MyVars.prefs.setBool(PrefsVars.CompletedTutorial.toString(), true);
      print(MyVars.prefs.getBool(PrefsVars.CompletedTutorial.toString()));

      Timer(Duration(seconds: 3), () {
        state.showSnackBar(SnackBar(
          content:
              Text('Hey, you seem new here! Do you want to see the tutorial?'),
          duration: Duration(seconds: 20),
          action: SnackBarAction(
            label: 'SEE THE TUTORIAL',
            onPressed: () => Navigator.pushReplacement(state.context,
                MaterialPageRoute(builder: (_) => TutorialPage())),
          ),
        ));
      });
    }
  }

  // Temporary
  void _showSubredditDialog(BuildContext context) {
    TextEditingController subredditTextController = TextEditingController();

    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              title: Text('Enter the name of the subreddit'),
              content: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text('r/'),
                    Padding(padding: EdgeInsets.only(right: 4.0)),
                    Expanded(
                        child: TextField(
                            controller: subredditTextController,
                            autofocus: true,
                            decoration: InputDecoration.collapsed(
                                hintText: 'The name of the subreddit')))
                  ]),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    MyVars.analytics.logEvent(
                        name: 'searchForSubreddit',
                        parameters: {
                          'subredditName': subredditTextController.text.trim()
                        });

                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (_) => NewFeedPage(
                                subreddit:
                                    subredditTextController.text.trim())));
                  },
                  child: Text('GO TO SUBREDDIT',
                      style: TextStyle(color: MyColors.accentColor)),
                )
              ],
            ));
  }
}
