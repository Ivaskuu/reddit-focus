import 'package:flutter/material.dart';
import 'package:zoomable_image/zoomable_image.dart';

import '../../my_vars.dart';

class FullscreenImagePage extends StatelessWidget {
  final String imageUrl;
  final String heroTag;
  FullscreenImagePage(this.imageUrl, this.heroTag);

  @override
  Widget build(BuildContext context) {
    MyVars.analytics.setCurrentScreen(screenName: 'FullscreenImagePage');
    MyVars.analytics.logEvent(name: 'showZoomableImage');

    return Scaffold(
      backgroundColor: Colors.black,
      body: GestureDetector(
        onVerticalDragEnd: (details) {
          print(details.velocity);
        },
        child: Stack(
          children: <Widget>[
            Hero(
                tag: heroTag,
                child: SizedBox.expand(
                    child: ZoomableImage(NetworkImage(imageUrl)))),
            Align(
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  AppBar(
                      backgroundColor: Colors.black12,
                      elevation: 0.0,
                      actions: [
                        IconButton(
                            icon: Icon(Icons.crop_free),
                            onPressed: () => Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => FullscreenImagePage(
                                        imageUrl, heroTag)))),
                        IconButton(
                            onPressed: () => Scaffold.of(context).showSnackBar(
                                SnackBar(
                                    content:
                                        Text('Will be implemented soon...'))),
                            icon: Icon(Icons.save)),
                      ])
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
