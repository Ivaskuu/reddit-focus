import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../misc/feed_model.dart';
import '../../my_vars.dart'; //PORCODIO QUESTO CODICE FA SCHIFO
import '../../misc/mycolors.dart';
import '../../logic/post.dart';

import 'fullscreen_image_page.dart';
import 'widgets/video_player_widget.dart';

import 'dart:math';
import 'package:url_launcher/url_launcher.dart';

class PostCard extends StatelessWidget {
  final int position;
  final int gradient;
  String heroTag;

  PostCard(this.position, this.gradient) {
    final Random rand = Random();
    heroTag = (DateTime.now().millisecondsSinceEpoch + rand.nextInt(100000))
        .toString();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<FeedModel>(builder: (_, __, model) {
      if (!model.initialized) {
        return _postCard(Container());
      } else {
        final Post post = model.posts[model.currentPostPos + position] ?? null;

        if (post == null) {
          return Container();
        } else if (post.nsfw) {
          return _postCard(
              InkWell(
                onTap: () => model.showNsfwMedia(),
                child: Center(
                  child: Container(
                    padding: EdgeInsets.all(8.0),
                    child: Material(
                      borderRadius: BorderRadius.circular(12.0),
                      color: MyColors.backgroundColor,
                      child: Container(
                        margin: EdgeInsets.all(12.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text('NSFW',
                                style: TextStyle(
                                    fontSize: 32.0,
                                    color: MyColors.importantTitle,
                                    fontWeight: FontWeight.w700)),
                            Text('Tap to reveal',
                                style:
                                    TextStyle(color: MyColors.importantTitle)),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              useGradient: true);
        } else {
          switch (post.urlType) {
            case PostUrlType.Image:
              if (!post.download)
                return _downloadPost(context, true, model);
              else
                return _imagePost(context, post);
              break;
            case PostUrlType.Video:
              if (!post.download)
                return _downloadPost(context, false, model);
              else
                return _videoPost(post);
              break;
            case PostUrlType.Link:
              return _linkPost(post, model);
              break;
            default:
              return _textPost();
              break;
          }
        }
      }
    });
  }

  Widget _linkPost(Post post, FeedModel model) {
    MyVars.analytics
        .logEvent(name: 'newContentCard', parameters: {'cardType': 'link'});

    if (post.url.contains('imgur')) print(post.url);

    IconData icon = Icons.language;
    if (post.url.contains('imgur'))
      icon = Icons.image;
    else if (post.url.contains('v.redd.it') || post.url.contains('gfycat'))
      icon = Icons.videocam;

    return Material(
        elevation: 4.0,
        borderRadius: BorderRadius.circular(12.0),
        child: Container(
          decoration: BoxDecoration(
              gradient: gradient != null
                  ? MyColors.contentCardsGradients[gradient]
                  : _emptyStateGradient()),
          child: InkWell(
            onTap: () => launch(post.url),
            onDoubleTap: () => model.upvotePost(),
            child: Center(
              child: Container(
                margin: EdgeInsets.all(16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Icon(icon, color: Colors.white, size: 80.0),
                    Padding(padding: EdgeInsets.only(bottom: 24.0)),
                    Text('Click to open: ' + post.url.toString(),
                        maxLines: 3,
                        overflow: TextOverflow.fade,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white)),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  Widget _videoPost(Post post) {
    MyVars.analytics
        .logEvent(name: 'newContentCard', parameters: {'cardType': 'video'});
    return _postCard(VideoPlayerWidget(post));
  }

  Widget _imagePost(BuildContext context, Post post) {
    MyVars.analytics
        .logEvent(name: 'newContentCard', parameters: {'cardType': 'image'});

    return _postCard(
        InkWell(
          onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => FullscreenImagePage(post.url, heroTag))),
          child: Hero(
              tag: heroTag,
              child: Container(
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: NetworkImage(post.url), fit: BoxFit.cover)))

              /* CachedNetworkImage
                (
                  fadeInDuration: const Duration(milliseconds: 100),
                  fadeOutDuration: const Duration(milliseconds: 100),
                  fit: BoxFit.cover,
                  alignment: Alignment.topCenter,
                  imageUrl: post.url,
                  placeholder: Padding
                  (
                    padding: const EdgeInsets.all(64.0),
                    child: Column
                    (
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[ CircularProgressIndicator() ],
                    )
                  ),
                ), */
              ),
        ),
        useGradient: false);
  }

  Widget _textPost() {
    MyVars.analytics
        .logEvent(name: 'newContentCard', parameters: {'cardType': 'text'});
    return _postCard(
        Icon(Icons.font_download, color: Colors.white, size: 80.0));
  }

  Widget _postCard(Widget child, {bool useGradient: true}) {
    return ScopedModelDescendant<FeedModel>(builder: (_, __, model) {
      return Material(
          elevation: 4.0,
          borderRadius: BorderRadius.circular(12.0),
          child: InkWell(
            onDoubleTap: () => model.upvotePost(),
            child: Container(
                decoration: useGradient
                    ? BoxDecoration(
                        gradient: gradient != null
                            ? MyColors.contentCardsGradients[gradient]
                            : _emptyStateGradient())
                    : BoxDecoration(),
                child: child),
          ));
    });
  }

  Widget _downloadPost(
      BuildContext context, bool imageOrVideo, FeedModel model) {
    return _postCard(
        InkWell(
            onTap: () => model.downloadMedia(), // TODO
            child: Center(
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                  Icon(Icons.file_download, color: Colors.white, size: 48.0),
                  Padding(padding: EdgeInsets.only(bottom: 16.0)),
                  Text(
                      'Tap to download the ' +
                          (imageOrVideo ? 'image' : 'video'),
                      style: TextStyle(color: Colors.white))
                ]))),
        useGradient: true);
  }

  LinearGradient _emptyStateGradient() {
    return LinearGradient(
        colors: [Colors.grey[300], Colors.grey[400]],
        end: Alignment.topLeft,
        begin: Alignment.bottomRight);
  }
}
