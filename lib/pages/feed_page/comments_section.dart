import 'dart:async';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../misc/feed_model.dart';
import '../../logic/user_comment.dart';
import '../../logic/post.dart';
import '../../misc/mycolors.dart';
import '../../misc/reddit_api.dart';
import '../../my_vars.dart';
import 'widgets/comment_piece.dart';
import 'widgets/quick_reply_dialog.dart';

class CommentsSection extends StatefulWidget {
  @override
  CommentsSectionState createState() => CommentsSectionState();
}

/// It needs to have [AutomaticKeepAliveClientMixin] to keep
/// the state active even if it is off-screen.
class CommentsSectionState extends State<CommentsSection>
    with AutomaticKeepAliveClientMixin<CommentsSection> {
  @override
  bool get wantKeepAlive => true;

  List<UserComment> comments;
  FeedModel m;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<FeedModel>(
      builder: (_, __, model) {
        if (model.initialized && model.actualPost.comments != null) {
          m = model;
          _buildComments(model.actualPost);
          return _commentsList(model.actualPost.opName);
        }
        return _empyState();
      },
    );
  }

  /// Count the number of comments for this post
  void _buildComments(Post actualPost) {
    comments = [];

    for (var comment in actualPost.comments) {
      comments.add(comment);
      if (comment.replies != null) {
        for (var reply in comment.replies) {
          comments.add(reply);
        }
      }
    }
  }

  Widget _commentsList(String opName) {
    /// Return a builder [SliverList] so comments build only when drawn
    return SliverList(
        delegate: SliverChildBuilderDelegate(
      (_, int i) {
        if (i == 0) {
          return Container(
              margin: EdgeInsets.only(top: 12.0),
              child: Text('${comments.length} COMMENTS',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: MyColors.subimportantTitle,
                      fontSize: 12.0)));
        } else {
          i = i - 1; // To be able to show comments num text

          return CommentPiece(
            comments[i],
            opName,
            onPress: () => comments[i].foldType == FoldType.Parent
                ? _foldReplies(comments[i])
                : !MyVars.readOnly ? showCommentOptions(comments[i]) : null,
            onDoubleTap: () => comments[i].foldType == FoldType.Parent
                ? null
                : _doubleTapToUpvote(comments[i]),
            onLongPress: () => _foldReplies(comments[i]),
          );
        }
      },
      childCount: comments.length + 1,
    ));
  }

  /// Folds the comments after the [commentToFold]
  /// if not foleded and vice-versa.
  void _foldReplies(UserComment commentToFold) {
    setState(() {
      if (commentToFold.foldType == FoldType.None) {
        commentToFold.foldType = FoldType.Parent;
        int i = comments.indexOf(commentToFold) + 1;
        while (comments[i].runtimeType == Reply &&
            comments[i].depth > commentToFold.depth) {
          comments[i].foldType = FoldType.Folded;
          i++;
        }
      } else if (commentToFold.foldType == FoldType.Parent) {
        commentToFold.foldType = FoldType.None;
        int i = comments.indexOf(commentToFold) + 1;
        while (comments[i].runtimeType == Reply &&
            comments[i].depth > commentToFold.depth) {
          comments[i].foldType = FoldType.None;
          i++;
        }
      }
    });
  }

  void showCommentOptions(UserComment c) {
    MyVars.analytics.logEvent(name: 'showCommentOptions');

    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext sheetContext) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(26.0),
                  child: Text('"' + c.content + '"',
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w400))),
              Expanded(
                  child: GridView.count(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                crossAxisCount: 3,
                children: <Widget>[
                  bottomSheetActionButton(
                      Icons.favorite, 'Upvote', sheetContext, () {
                    if (c.voteStatus != true) {
                      MyVars.analytics.logEvent(
                          name: 'voteComment',
                          parameters: {'from': 'commentOptionsDialog'});

                      setState(() {
                        if (c.upvotesNum !=
                            null) // If the upvotes num is hidden
                        {
                          if (c.voteStatus == null)
                            c.upvotesNum++;
                          else
                            c.upvotesNum += 2;
                        }

                        c.voteStatus = true;
                      });
                      RedditApi.voteComment(c.id, c.voteStatus);
                    }
                  }),
                  bottomSheetActionButton(
                      Icons.thumb_down, 'Downvote', sheetContext, () {
                    if (c.voteStatus != false) {
                      MyVars.analytics.logEvent(
                          name: 'voteComment',
                          parameters: {'from': 'commentOptionsDialog'});

                      setState(() {
                        if (c.upvotesNum != null) {
                          if (c.voteStatus == null)
                            c.upvotesNum--;
                          else
                            c.upvotesNum -= 2;
                        }
                        c.voteStatus = false;
                      });
                      RedditApi.voteComment(c.id, c.voteStatus);
                    }
                  }),
                  bottomSheetActionButton(
                      Icons.thumbs_up_down, 'Clear vote', sheetContext, () {
                    if (c.voteStatus != null) {
                      MyVars.analytics.logEvent(
                          name: 'voteComment',
                          parameters: {'from': 'commentOptionsDialog'});

                      setState(() {
                        if (c.upvotesNum != null) {
                          if (c.voteStatus)
                            c.upvotesNum--;
                          else
                            c.upvotesNum++;
                        }
                        c.voteStatus = null;
                      });
                      RedditApi.voteComment(c.id, c.voteStatus);
                    }
                  }),
                  bottomSheetActionButton(
                      Icons.reply,
                      'Reply',
                      sheetContext,
                      () => showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (_) {
                            return Align(
                                alignment: Alignment.bottomCenter,
                                child: QuickReplyDialog(m, replyingTo: c));
                          })),
                  bottomSheetActionButton(
                      Icons.flag, 'Flag', sheetContext, null),
                  bottomSheetActionButton(
                      Icons.save, 'Save', sheetContext, null),
                ],
              ))
            ],
          );
        });
  }

  void _doubleTapToUpvote(UserComment c) {
    setState(() {
      if (c.voteStatus != null) {
        MyVars.analytics
            .logEvent(name: 'voteComment', parameters: {'from': 'doubleTap'});

        if (c.upvotesNum != null) {
          if (c.voteStatus == false)
            c.upvotesNum++;
          else
            c.upvotesNum--;
        }
        c.voteStatus = null;

        RedditApi.voteComment(c.id, c.voteStatus);
      } else {
        MyVars.analytics
            .logEvent(name: 'voteComment', parameters: {'from': 'doubleTap'});

        if (c.upvotesNum != null) {
          if (c.voteStatus == null)
            c.upvotesNum++;
          else
            c.upvotesNum += 2;
        }
        c.voteStatus = true;

        RedditApi.voteComment(c.id, c.voteStatus);
      }
    });
  }

  Widget bottomSheetActionButton(IconData icon, String action,
      BuildContext sheetContext, Function callback) {
    return InkWell(
        onTap: () {
          Navigator.pop(sheetContext);
          if (callback != null) callback();
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(icon,
                size: 48.0,
                color: callback != null
                    ? MyColors.importantTitle
                    : MyColors.subimportantTitle),
            Center(child: Text(action)),
          ],
        ));
  }

  /// Called by [QuickReplyDialog] when the user posts a comment to the submission
  void addNewCommentToHierarchy(UserComment c, {UserComment replyingTo}) {
    setState(() {
      if (replyingTo != null) {
        int index = comments.indexOf(replyingTo);
        comments.insert(index + 1, c);

        // TODO: When changing post and returning to this one
        // the new comment won't be saved

        /* if (index != -1) // Reply of the comment parent
        {
          c.depth = actualPost.comments[index].depth + 1;

          if (actualPost.comments[index].replies == null)
            actualPost.comments[index].replies = List();

          actualPost.comments[index].replies.insert(0, c);
          _updateComments();
        } else // The parent of the comment is a reply of a comment
        {
          for (int i = 0; i < actualPost.comments.length; i++) {
            if (actualPost.comments[i].replies != null) {
              int index = actualPost.comments[i].replies.indexOf(replyingTo);
              if (index != -1) {
                c.depth = actualPost.comments[i].replies[index].depth + 1;
                actualPost.comments[i].replies.insert(index + 1, c);
                _updateComments();
              }
            }
          }
        } */
      } else {
        comments.insert(0, c);
      }
    });
  }

  Widget _empyState() {
    return SliverToBoxAdapter(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
          Padding(padding: EdgeInsets.only(bottom: 10.0)),
          loadingCommentWidget(),
          loadingCommentWidget(),
          loadingCommentWidget(),
          loadingCommentWidget(),
          loadingCommentWidget(),
          loadingCommentWidget(),
          loadingCommentWidget(),
          loadingCommentWidget()
        ]));
  }

  Widget loadingCommentWidget() {
    return Container(
        margin: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 16.0),
        color: Colors.black12,
        child: SizedBox.fromSize(size: Size.fromHeight(16.0)));
  }
}
