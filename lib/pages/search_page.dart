import 'package:flutter/material.dart';
import '../misc/mycolors.dart';
import '../my_vars.dart';

class SearchPage extends StatefulWidget
{
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage>
{
  @override
  void initState()
  {
    super.initState();
    MyVars.analytics.setCurrentScreen(screenName: 'SearchPage');
  }

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
    (
      appBar: PreferredSize
      (
        preferredSize: Size(0.0, 0.0),
        child: Container
        (
          decoration: BoxDecoration(gradient: MyColors.searchBarColors),
        ),
      ),
      body: ListView
      (
        children: <Widget>
        [
          SizedBox.fromSize // Appbar with the search textField
          (
            size: Size.fromHeight(190.0),
            child: Stack
            (
              children: <Widget>
              [
                Container // Appbar background color
                (
                  margin: EdgeInsets.only(bottom: 42.0),
                  child: Align
                  (
                    alignment: FractionalOffset.topCenter,
                    child: Material
                    (
                      elevation: 0.0,
                      child: Container
                      (
                        decoration: BoxDecoration
                        (
                          gradient: MyColors.searchBarColors
                        )
                      )
                    ),
                  )
                ),
                Align // Appbar with the close iconbutton
                (
                  alignment: FractionalOffset.topCenter,
                  child: AppBar
                  (
                    backgroundColor: Colors.transparent,
                    elevation: 0.0,
                    leading: IconButton
                    (
                      onPressed: () => Navigator.of(context).pop(),
                      icon: Icon(Icons.close, color: Colors.white70)
                    ),
                  ),
                ),
                Align // Search text
                (
                  alignment: FractionalOffset.center,
                  child: Container
                  (
                    margin: EdgeInsets.only(bottom: 56.0),
                    child: Text('SEARCH', style: Theme.of(context).textTheme.title.copyWith(color: Colors.white, fontSize: 26.0, fontWeight: FontWeight.w600)),
                  )
                ),
                Container // Search textField
                (
                  margin: EdgeInsets.all(16.0),
                  child: Align
                  (
                    alignment: FractionalOffset.bottomCenter,
                    child: Material
                    (
                      elevation: 8.0,
                      color: MyColors.backgroundColor,
                      borderRadius: BorderRadius.circular(2.0),
                      child: Container
                      (
                        margin: EdgeInsets.all(16.0),
                        child: Row
                        (
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>
                          [
                            Container(margin: EdgeInsets.only(left: 8.0, right: 16.0), child: Icon(Icons.search)),
                            Expanded
                            (
                              child: TextField
                              (
                                autofocus: true,
                                autocorrect: false,
                                decoration: InputDecoration.collapsed
                                (
                                  hintText: "What do you want to search for?",
                                ),
                                onChanged: (_) => setState(() {}),
                              )
                            )
                          ]
                        )
                      ),
                    ),
                  )
                )
              ]
            )
          ),
          Container // Results text
          (
            margin: EdgeInsets.only(left: 10.0, top: 10.0),
            child: Text('Results (367)', style: TextStyle(fontWeight: FontWeight.w700, color: MyColors.subimportantTitle, fontSize: 16.0)),
          ),
          SearchResultCard(),
          SearchResultCard(),
          SearchResultCard(),
          SearchResultCard(),
          SearchResultCard(),
        ],
      )
    );
  }
}

class SearchResultCard extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Container
    (
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      child: Card
      (
        child: InkWell
        (
          onTap: () => null,
          child: Container
          (
            margin: EdgeInsets.all(4.0),
            child: ListTile
            (
              leading: Image.asset('res/focus.jpg', fit: BoxFit.cover),
              title: Text('The title of the post'),
              subtitle: Text('The description of the post if it has one...'),
              trailing: Column
              (
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  Icon(Icons.arrow_upward, size: 16.0),
                  Padding(padding: EdgeInsets.only(right: 4.0)),
                  Text('3.6k'),
                ],
              ),
            )
          ),
        )
      ),
    );
  }
}