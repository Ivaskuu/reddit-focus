import 'package:flutter/material.dart';
import 'dart:async';
import '../my_vars.dart';
import '../misc/mycolors.dart';
import 'package:url_launcher/url_launcher.dart';

List<List<String>> _changeLog = [
  [
    'v0.0.8',
    'Images, gifs and videos are now showed in the app',
    'Big performance improvement (comments that aren\'t visible to the screen aren\'t drawn',
    'animated login page',
    'Reply to comments and add a comment to a post',
    'Tutorial when first opening the app (finally :D)',
    'icon',
    'A show more comments button (that doesn\'t work yet)',
    'Started working on the Settings page',
    'Changelog menu button in the settings page',
  ],
  ['v0.0.7.1', 'Removed an odd bug that slipped in the last version...'],
  [
    'v0.0.7',
    'Fixed a critical app where the app would crash if the first post had the title too long.',
    'You can now fold comments! Just longpress one to see it and its children folding.',
    'Upvote comments by clicking on them, and show more options on double-tapping a comment.',
    'Double tap a post card to upvote it, and double tap once again to cancel the upvote.',
    'Click on the post upvotes circle button to reveal more options.'
  ]
];
String _privacyPolicyUrl =
    r'https://v3pi0smvrkivz6xdjuzj1w-on.drv.tw/focus_reddit_webpages/privacy_policy.html';
String _termsAndConditions =
    r'https://v3pi0smvrkivz6xdjuzj1w-on.drv.tw/focus_reddit_webpages/terms_and_conditions.html';

class SettingsPage extends StatefulWidget {
  @override
  SettingsPageState createState() => SettingsPageState();
}

class SettingsPageState extends State<SettingsPage> {
  @override
  void initState() {
    super.initState();
    MyVars.analytics.setCurrentScreen(screenName: 'SettingsPage');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: MyColors.backgroundColor,
        elevation: 0.0,
        automaticallyImplyLeading: false,
        title: Text('Settings',
            overflow: TextOverflow.fade,
            style: TextStyle(
                color: MyColors.importantTitle,
                fontWeight: FontWeight.w800,
                fontSize: 28.0)),
        leading: IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(Icons.close, color: MyColors.importantTitle)),
      ),
      backgroundColor: MyColors.backgroundColor,
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(16.0),
            child: Text(
                'This settings menu is in construction and nothing works for the moment.'),
          ),
          _categoryText('General'),
          ListTile(
            onTap: () => _showChangelogDialog(),
            leading: Icon(Icons.restore),
            title: Text('Changelog'),
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            title: Text('App infos & legalese'),
            subtitle: Text('Current version: Alpha 0.0.11'),
            onTap: () => showAboutDialog(
                    context: context,
                    applicationName: 'Focus for Reddit',
                    applicationVersion: 'Alpha 0.0.11',
                    children: <Widget>[
                      GestureDetector(
                        onTap: () => launch(_privacyPolicyUrl),
                        child: Text('Privacy policy',
                            style: TextStyle(
                                color: Colors.blue,
                                decoration: TextDecoration.underline)),
                      ),
                      GestureDetector(
                        onTap: () => launch(_termsAndConditions),
                        child: Text('Terms and conditions',
                            style: TextStyle(
                                color: Colors.blue,
                                decoration: TextDecoration.underline)),
                      ),
                    ]),
          ),
          ListTile(
            onTap: () => showGitterDialog(context),
            leading: Icon(Icons.question_answer),
            title: Text('Join Gitter chat'),
          ),
          ListTile(
            onTap: () => launch(r'https://trello.com/b/Kyj17iHr'),
            leading: Icon(Icons.dashboard),
            title: Text('Trello board'),
            subtitle: Text('See the changes happening to the app in real time'),
          ),
          _categoryText('Performance'),
          SwitchListTile(
            onChanged: (bool newValue) => setState(() =>
                MyVars.setPrefsVar(PrefsVars.PostCardsShadow, value: newValue)),
            value: MyVars.getPrefsVar(PrefsVars.PostCardsShadow),
            activeColor: MyColors.importantTitle,
            secondary: Icon(Icons.cloud),
            title: Text('Enable cards shadow'),
          ),
          ListTile(
            leading: Icon(Icons.get_app),
            title: Text('Max post photo resolution'),
            subtitle: Text('Unlimited'),
          ),
          _categoryText('Feeds'),
          SwitchListTile(
            isThreeLine: true,
            onChanged: (bool newValue) => setState(() => MyVars
                .setPrefsVar(PrefsVars.SwipeToNextPostOnVote, value: newValue)),
            value: MyVars.getPrefsVar(PrefsVars.SwipeToNextPostOnVote),
            activeColor: MyColors.importantTitle,
            secondary: Icon(Icons.art_track),
            title: Text('Auto-swipe posts on vote'),
            subtitle:
                Text('The app will go to the next post when you vote a post'),
          ),
          SwitchListTile(
            isThreeLine: true,
            onChanged: (bool newValue) => setState(() => MyVars
                .setPrefsVar(PrefsVars.AutoDownloadMedia, value: newValue)),
            value: MyVars.getPrefsVar(PrefsVars.AutoDownloadMedia),
            activeColor: MyColors.importantTitle,
            secondary: Icon(Icons.get_app),
            title: Text('Auto-download media on cellular data'),
            subtitle: Text(
                'If disabled, you have to tap on the post to download the media'),
          ),
          SwitchListTile(
            isThreeLine: true,
            onChanged: (bool newValue) => setState(
                () => MyVars.setPrefsVar(PrefsVars.HideNSFW, value: newValue)),
            value: MyVars.getPrefsVar(PrefsVars.HideNSFW),
            activeColor: MyColors.importantTitle,
            secondary: Icon(Icons.blur_on),
            title: Text('Hide NSFW'),
            subtitle: Text(
                'If enabled, you will have to tap the post card to see its NSFW content'),
          ),
          _categoryText('Theme'),
          SwitchListTile(
            onChanged: (bool newValue) => null,
            value: true,
            activeColor: MyColors.importantTitle,
            secondary: Icon(Icons.brightness_7),
            title: Text('Day mode enabled'),
            subtitle: Text('Bright colors'),
          ),
          SwitchListTile(
            onChanged: (bool newValue) => null,
            value: true,
            activeColor: MyColors.importantTitle,
            secondary: Icon(Icons.brightness_2),
            title: Text('Night mode enabled'),
            subtitle: Text('Dark colors'),
          ),
        ],
      ),
    );
  }

  void _showChangelogDialog() {
    List<Widget> changelogWidgets = List();
    for (int i = 0; i < _changeLog.length; i++) {
      changelogWidgets.add(
          Text(_changeLog[i][0], style: Theme.of(context).textTheme.title));
      for (int j = 1; j < _changeLog[i].length; j++) {
        changelogWidgets.add(Text(' * ' + _changeLog[i][j]));
      }
      changelogWidgets.add(Padding(padding: EdgeInsets.only(bottom: 16.0)));
    }

    showDialog(
        context: context,
        child: SimpleDialog(
            contentPadding: EdgeInsets.all(16.0),
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(bottom: 16.0),
                  child: Text('Changelog',
                      style: Theme
                          .of(context)
                          .textTheme
                          .title
                          .copyWith(color: MyColors.importantTitle))),
            ]..addAll(changelogWidgets)));
  }

  Widget _categoryText(String category) {
    return Container(
      margin: EdgeInsets.only(left: 16.0, top: 16.0, bottom: 8.0),
      child: Text(category,
          style: TextStyle(
              color: MyColors.accentColor,
              fontWeight: FontWeight.w700,
              fontSize: 14.0)),
    );
  }

  void showGitterDialog(BuildContext context) {
    showDialog(
        context: context,
        child: SimpleDialog(
          contentPadding: EdgeInsets.all(0.0),
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 24.0),
              color: Colors.pink,
              child: Icon(Icons.chat, color: Colors.white, size: 72.0),
            ),
            Container(
                margin: EdgeInsets.all(24.0),
                child: Text('Join the Gitter chat! 😀',
                    style: Theme
                        .of(context)
                        .textTheme
                        .title
                        .copyWith(color: MyColors.importantTitle))),
            Container(
                margin: EdgeInsets.symmetric(horizontal: 24.0),
                child: Text(
                    "Submit bugs, propose improvements and ideas, chat with me and get feedback of the app progress.")),
            Container(
                margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 24.0),
                child: squaredButton("OPEN IN BROWSER", context)),
            Container(
                margin: EdgeInsets.all(8.0),
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);

                    MyVars.analytics.logEvent(name: 'closeGitterChatDialog');
                    MyVars.prefs
                        .setBool(PrefsVars.ShowGitterDialog.toString(), true);
                  },
                  child: Text('No, thanks'),
                )),
          ],
        ));
  }

  Widget squaredButton(String btnText, BuildContext context) {
    return Container(
        child: Material(
            elevation: 8.0,
            borderRadius: BorderRadius.circular(2.0),
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
                MyVars.analytics.logEvent(name: 'openGitterChat');
                launch(r'https://gitter.im/Focus-for-reddit/Lobby');
              },
              child: Container(
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(2.0),
                  gradient: LinearGradient(colors: <Color>[
                    Color.fromARGB(255, 93, 107, 232),
                    Color.fromARGB(255, 89, 182, 191)
                  ]),
                ),
                child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 4.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.public, color: Colors.white),
                        Padding(padding: EdgeInsets.only(right: 16.0)),
                        Text(btnText.toUpperCase(),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ],
                    )),
              ),
            )));
  }
}
