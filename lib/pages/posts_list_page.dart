import 'package:flutter/material.dart';
import '../misc/mycolors.dart';
import '../logic/post.dart';
import '../misc/feed_model.dart';

import 'feed_page/widgets/video_player_widget.dart';
import 'package:scoped_model/scoped_model.dart';
import 'dart:math';

class PostsListPage extends StatelessWidget {
  FeedModel m;
  PostsListPage(this.m);

  @override
  Widget build(BuildContext context) {
    print(context);

    return ScopedModel(
        model: m,
        child: ScopedModelDescendant<FeedModel>(
          builder: (context, child, model) => model.postsListVisible
              ? SizedBox.expand(
                  child: Container(
                    color: Colors.black87,
                    child: model != null
                        ? model.currentPostPos > 0
                            ? ListView.builder(
                                reverse: true,
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemBuilder: (_, int i) => PostTile(
                                    model.posts[model.currentPostPos - i - 1],
                                    model.currentPostPos - i - 1),
                                itemCount: model.currentPostPos,
                              )
                            : Center(
                                child: Padding(
                                padding: EdgeInsets.all(16.0),
                                child: Text(
                                    'Already seen posts will be added here', textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white)),
                              ))
                        : Container(),
                  ),
                )
              : Container(),
        ));
  }
}

class PostTile extends StatelessWidget {
  final Post post;
  final int ith;
  PostTile(this.post, this.ith) : super(key: Key(post.id));

  @override
  Widget build(BuildContext context) {
    if (post.urlType == null || post.urlType == PostUrlType.Link) {
      return _buildContentCard(
          ith,
          Container(
            padding: EdgeInsets.all(16.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text('r/${post.subredditName}',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: MyColors.accentColor,
                        fontWeight: FontWeight.w800,
                        fontSize: 12.0)),
                Padding(padding: EdgeInsets.only(bottom: 4.0)),
                Text(post.title,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500)),
              ],
            ),
          ));
    } else {
      // Post with media
      return _buildContentCard(
          ith,
          SizedBox.fromSize(
            size: Size.fromHeight(200.0),
            child: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                post.urlType == PostUrlType.Image
                    ? Image.network(post.url, fit: BoxFit.cover)
                    : VideoPlayerWidget(post),

                /// Shadow to see better the text
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Colors.transparent, Colors.black87],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter)),
                ),

                /// Post title and subreddit text
                Container(
                  padding: EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('r/${post.subredditName}',
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: MyColors.accentColor,
                              fontWeight: FontWeight.w800,
                              fontSize: 12.0)),
                      Padding(padding: EdgeInsets.only(bottom: 2.0)),
                      Text(post.title,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white)),
                    ],
                  ),
                ),
              ],
            ),
          ));
    }
  }

  Widget _buildContentCard(int ith, Widget child) {
    return InkWell(
      onTap: () {
        modelsHierarchyList.last.feedModel.goToPost(ith);
        modelsHierarchyList.last.feedModel.closePostsList();
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
        child: Material(
          color: MyColors.backgroundColor,
          elevation: 4.0,
          borderRadius: BorderRadius.circular(8.0),
          child: child,
        ),
      ),
    );
  }
}
