import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

import 'particles_page.dart';
import '../splash_screen.dart';
import '../misc/reddit_api.dart';
import '../my_vars.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool showBrowser = false;
  bool isLoading = false;
  final webView = FlutterWebviewPlugin();

  String url;
  String loginUrl;
  String registerUrl = r'https://www.reddit.com/register';

  @override
  void initState() {
    if(MyVars.prefs == null) MyVars.initPrefs();

    super.initState();
    RedditApi.initReddit();

    webView.onUrlChanged.listen((String url) {
      if (url.contains('&code=')) {
        print('>> Authorized <<');

        // Close the webview
        setState(() {
          showBrowser = false;
          isLoading = true;
        });

        // Save the code to the shared preferences
        String code = url.split('&code=')[1];
        RedditApi.authorizeUser(code).then((bool wasAuthorized) {
          if (wasAuthorized) {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (_) => SplashScreen()));
          } else
            showDialog(
                context: context,
                builder: (_) => AlertDialog(
                      title: Text('There was an error :-('),
                      content: Text(
                          'The login operation didn\'t end successfully. Please check your internet connection and try again.'),
                      actions: <Widget>[
                        FlatButton(
                            onPressed: () => Navigator.of(context).pop(),
                            child: Text('TRY AGAIN'))
                      ],
                    ));
        });
      }
    });

    MyVars.analytics.setCurrentScreen(screenName: 'LoginPage');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red,
      body: showBrowser
          ? webBrowser()
          : Stack(
              children: <Widget>[
                AnimatedBackground(DemoPage()),
                SizedBox.expand(
                  child: Container(color: Colors.black54),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Focus for Reddit',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 40.0,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white)),
                          Padding(padding: EdgeInsets.only(bottom: 16.0)),
                          Text(
                              'Welcome aboard!\nFocus let\'s you browse Reddit in a totally manner.',
                              style: TextStyle(
                                  fontSize: 26.0,
                                  fontWeight: FontWeight.w200,
                                  color: Colors.white)),
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 32.0),
                          child: loginButton(),
                        ),
                        Padding(padding: EdgeInsets.only(bottom: 12.0)),
                        Container(
                            margin: EdgeInsets.symmetric(horizontal: 32.0),
                            child: GestureDetector(
                              onTap: () {
                                url = registerUrl;
                                setState(() => showBrowser = true);
                              },
                              child: Text('Don\'t have an account? REGISTER.',
                                  style: TextStyle(color: Colors.white70)),
                            ))
                      ],
                    ),
                  ],
                ),

                /// Loading bar
                isLoading
                    ? SizedBox.expand(
                        child: Container(color: Colors.black54),
                      )
                    : Container(),
                isLoading
                    ? Center(child: CircularProgressIndicator())
                    : Container(),
              ],
            ),
    );
  }

  Widget loginButton() {
    return Material(
      color: Colors.black,
      elevation: 10.0,
      borderRadius: BorderRadius.circular(8.0),
      child: InkWell(
        onTap: () {
          if (loginUrl == null) loginUrl = RedditApi.getAuthUrl();
          url = loginUrl;
          setState(() => showBrowser = true);
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0),
          child: ListTile(
            leading: Icon(Icons.account_circle, color: Colors.white),
            title: Text('Login with Reddit',
                style: TextStyle(color: Colors.white)),
            trailing: Icon(Icons.arrow_forward, color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget webBrowser() {
    return WebviewScaffold(
      appBar: AppBar(
        primary: true,
        leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: () => setState(() => showBrowser = false)),
        title: Text('https://www.reddit.com/...'),
        backgroundColor: Colors.blueGrey,
      ),
      url: url,
      clearCache: true,
      clearCookies: true,
      withZoom: true,
    );
  }
}

class AnimatedBackground extends StatefulWidget {
  final Widget child;
  AnimatedBackground(this.child);

  @override
  AnimatedBackgroundState createState() => AnimatedBackgroundState();
}

class AnimatedBackgroundState extends State<AnimatedBackground>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  final ColorTween topColor =
      ColorTween(begin: Colors.black, end: Color(0xFF84fab0));
  final ColorTween bottomColor =
      ColorTween(begin: Color(0xFF434343), end: Color(0xFF8fd3f4));

  final Tween<double> particlesOpacity = Tween(begin: 0.0, end: 1.0);
  bool animateParticlesOpacity = true;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(duration: Duration(seconds: 10), vsync: this);
    _controller.addListener(() => setState(() {}));
    _controller.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        if (animateParticlesOpacity)
          setState(() => animateParticlesOpacity = false);
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) _controller.forward();
    });

    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            topColor
                .animate(CurvedAnimation(
                    parent: _controller, curve: Curves.easeInOut))
                .value,
            bottomColor
                .animate(CurvedAnimation(
                    parent: _controller, curve: Curves.easeInOut))
                .value,
          ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
          child: Opacity(
              opacity: animateParticlesOpacity
                  ? particlesOpacity
                      .animate(CurvedAnimation(
                          parent: _controller, curve: Curves.easeIn))
                      .value
                  : 1.0,
              child: widget.child)),
    );
  }
}
