import 'package:flutter/material.dart';
import '../splash_screen.dart';
import '../my_vars.dart';
import 'dart:async';

class TutorialPage extends StatefulWidget {
  @override
  TutorialPageState createState() => TutorialPageState();
}

class TutorialPageState extends State<TutorialPage> {
  PageController _controller = PageController();
  int pageNum = 0;
  bool canNextPage = true;

  @override
  void initState() {
    super.initState();
    MyVars.analytics.setCurrentScreen(screenName: 'TutorialPage');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _controller,
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Center(
                      child: Container(
                        margin: EdgeInsets.all(48.0),
                        child: Text(
                            'Welcome to Focus for Reddit. This fast tutorial will teach you how to use the app.',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 20.0)),
                      ),
                    ),
                    _tutorialPage('res/tutorial_1.gif',
                        'The posts medias are shown as cards (image, link, video...).\nSwipe a card to the left or right, to go to the next post (think Tinder).'),
                    _tutorialPage('res/tutorial_2.gif',
                        'Double tap to upvote a post.\n\nAlso double tap on a comment to upvote it.'),
                    _tutorialPage('res/tutorial_3.gif',
                        'Tap the likes round button to show more options.\n\nYou can also click on a comment to reveal more options.'),
                    Container(
                        margin: EdgeInsets.all(48.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(top: 48.0),
                                child: Text(
                                    'Slide from the top to the bottom to see a list of posts you have already seen.',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20.0)),
                              )
                            ])),
                    Container(
                        margin: EdgeInsets.all(48.0),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              CircleAvatar(
                                  backgroundColor: Colors.green,
                                  child: Icon(Icons.done,
                                      color: Colors.white, size: 42.0),
                                  radius: 36.0),
                              Container(
                                margin: EdgeInsets.only(top: 48.0),
                                child: Text('Well done! Thank you for your patience.',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20.0)),
                              )
                            ]))
                  ]),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: pageNum != 0 && pageNum != 5
                        ? Text('$pageNum/4',
                            style: TextStyle(color: Colors.black54))
                        : Container(), // ${_controller.page}
                  ),
                  FlatButton(
                    onPressed: () => canNextPage
                        ? setState(() {
                            pageNum++;
                            canNextPage = false;
                            Timer(Duration(milliseconds: 350),
                                () => canNextPage = true);

                            if (pageNum >= 6) {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => SplashScreen()));
                            } else {
                              _controller.animateToPage(
                                  _controller.page.round() + 1,
                                  duration: Duration(milliseconds: 350),
                                  curve: Curves.easeIn);
                            }
                          })
                        : () => null,
                    child: Text('NEXT', style: TextStyle(color: Colors.black)),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  Widget _tutorialPage(String imgPath, String title) {
    return Column(
      children: <Widget>[
        Expanded(
          child: Image.asset(imgPath, fit: BoxFit.contain),
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 32.0, horizontal: 32.0),
          child: Text(title,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  fontSize: 20.0)),
        ),
      ],
    );
  }
}
