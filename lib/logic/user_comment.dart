enum FoldType { None, Folded, Parent }

class UserComment {
  String id;
  String content;
  String userName; // TODO: replace with User class

  int upvotesNum;
  bool voteStatus;
  int depth;
  DateTime unixTime;
  List<Reply> replies;

  FoldType foldType = FoldType.None;

  UserComment(this.content, this.userName, this.unixTime,
      {this.upvotesNum: -5,
      this.voteStatus: false,
      this.depth: 0,
      this.replies,
      this.id});
}

class Reply extends UserComment {
  Reply(String content, String userName, DateTime unixTime, int depth,
      {int upvotesNum: -5, bool voteStatus: false, String id})
      : super(content, userName, unixTime,
            upvotesNum: upvotesNum,
            voteStatus: voteStatus,
            depth: depth,
            id: id);
}

class MoreUserComment extends UserComment {
  MoreUserComment(int depth)
      : super(null, null, null,
            upvotesNum: null, voteStatus: null, depth: depth, id: null);
}
