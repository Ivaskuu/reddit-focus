import 'user_comment.dart';
import '../my_vars.dart';

enum PostUrlType { Image, Video, Link }

class Post {
  String title;
  String selfText;
  String url;

  int upvotes;
  bool voteStatus; // true => upvoted; false => downvoted; null => nothing
  String opName; // TODO: replace with user
  String subredditName;
  bool nsfw;
  bool download;

  int gradientColor;
  PostUrlType urlType;
  bool pinned;
  DateTime createdTime;
  int commentsNum;
  List<UserComment> comments; // To load comments

  String id;

  Post(this.title,
      {this.selfText,
      this.url,
      this.upvotes,
      this.voteStatus,
      this.opName,
      this.subredditName,
      this.nsfw,
      this.pinned,
      this.createdTime,
      this.commentsNum,
      this.gradientColor,
      this.id,
      this.download}) {
    download = MyVars.connectedToWifi ||
        MyVars.getPrefsVar(PrefsVars.AutoDownloadMedia);
    if (url != null && url.trim() != '') {
      if (url.endsWith('.png') ||
          url.endsWith('.jpg') ||
          url.endsWith('.gif') ||
          url.endsWith('.gifv') ||
          url.contains('imgur') && !url.contains('imgur.com/a')) {
        urlType = PostUrlType.Image;
        url = parseUrl(PostUrlType.Image, url);
      } else if (url.endsWith('.mp4') ||
          url.contains('gfycat') ||
          url.contains('v.redd.it')) {
        urlType = PostUrlType.Video;
        url = parseUrl(PostUrlType.Video, url);
      } else
        urlType = PostUrlType.Link;
    }
  }

  static String parseUrl(PostUrlType type, String uri) {
    if (type == PostUrlType.Image) {
      if (uri.contains('.gifv'))
        uri = uri.substring(0, uri.length - 1); // Remove the v from gifv
      else if (!uri.endsWith('.png') &&
          !uri.endsWith('.jpg') &&
          !uri.endsWith('.gif') &&
          !uri.endsWith('.gifv') &&
          uri.contains('imgur')) {
        int numOfBackslash = uri.split('/').length;
        uri = 'https://i.imgur.com/' +
            uri.split('/')[numOfBackslash - 1] +
            '.jpg';
      }
    } else if (type == PostUrlType.Video) {
      if (uri.contains('v.redd.it') && !uri.endsWith('DASH_600_K')) {
        if (!uri.endsWith('/')) uri += '/';
        uri += 'DASH_600_K';
      }
      // else if(gfycat) // can't do it (now) because of the future
    }

    return uri;
  }
}
