class RedditUser
{
  String id;
  String displayName;
  int karma;

  RedditUser(this.id, this.displayName, {this.karma});
}